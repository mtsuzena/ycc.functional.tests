import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadWaybill = require('../payloads/waybill.json');
const payloadWaybill2 = require('../payloads/waybill-2.json');
const payloadCommodity = require('../../commodity/payloads/commodity.json');
const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadCustomer = require('../../customer/payloads/customer.json');
const payloadCustomer2 = require('../../customer/payloads/customer-2.json');

const customertBaseUrl = 'Customer';
const YardBaseUrl = 'Yard';
const commodityBaseUrl = 'Commodity';
const WaybillBaseUrl = 'Waybill';
var token = '';

describe('POST Waybill', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, commodityBaseUrl, payloadCommodity);
            Requests.addMultiplePayloads(token, `${YardBaseUrl}/yards`, [payloadYard, payloadYard2]);
            Requests.addMultiplePayloads(token, customertBaseUrl+"/customers", [payloadCustomer, payloadCustomer2]);
        });
    });

    it('YCC-525 - [POST] - Creates a new waybill', () => {
        Requests.addPayload(token, WaybillBaseUrl, payloadWaybill).should((responseAddWaybill) => {
            expect(responseAddWaybill.status).to.eq(200);
        });
    });

    it('YCC-529 - [POST] - Creates multiple waybills', () => {
        Requests.addMultiplePayloads(token, `${WaybillBaseUrl}/waybills`, [payloadWaybill, payloadWaybill2]).should((responseAddMultipleWaybills) => {
            expect(responseAddMultipleWaybills.status).to.eq(200);
        });
    });

    afterEach(() => {
        Requests.deletePayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill.number}`);
    });


    after(() => {
        Requests.deletePayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill2.number}`);
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`);
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer2.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard2.code}`);
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`);
    });

});