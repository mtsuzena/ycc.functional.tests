import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadWaybill = require('../payloads/waybill.json');
const payloadWaybill2 = require('../payloads/waybill-2.json');
const payloadCommodity = require('../../commodity/payloads/commodity.json');
const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadCustomer = require('../../customer/payloads/customer.json');
const payloadCustomer2 = require('../../customer/payloads/customer-2.json');

const customertBaseUrl = 'Customer';
const YardBaseUrl = 'Yard';
const commodityBaseUrl = 'Commodity';
const WaybillBaseUrl = 'Waybill';

var token = '';

describe('GET Waybill', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, commodityBaseUrl, payloadCommodity);
            Requests.addMultiplePayloads(token, `${YardBaseUrl}/yards`, [payloadYard, payloadYard2]);
            Requests.addMultiplePayloads(token, customertBaseUrl+"/customers", [payloadCustomer, payloadCustomer2]);
            Requests.addMultiplePayloads(token, `${WaybillBaseUrl}/waybills`, [payloadWaybill, payloadWaybill2]);
        });
    });

    it('YCC-524 - [GET] - Get all waybills', () => {
        Requests.getAllPayloads(token, WaybillBaseUrl).should((responseGetAllWaybills) => {
            expect(responseGetAllWaybills.status).to.eq(200);
            expect(responseGetAllWaybills.body).to.be.not.null;

            var responsePayloadWaybill1 = '';
            var responsePayloadWaybill2 = '';
            responseGetAllWaybills.body.results.map(function(waybill, i) {
                if(payloadWaybill.number == waybill.number){
                    responsePayloadWaybill1 = waybill;
                }
                if(payloadWaybill2.number == waybill.number){
                    responsePayloadWaybill2 = waybill;
                }
            });

            expect(responsePayloadWaybill1).to.deep.equal(payloadWaybill);
            expect(responsePayloadWaybill2).to.deep.equal(payloadWaybill2);
        });
    });

    it('YCC-526 - [GET] - Get waybill by number', () => {
        Requests.getPayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill.number}`).should((responseGetWaybill) => {
            expect(responseGetWaybill.status).to.eq(200);
            expect(responseGetWaybill.body).to.be.not.null;
            expect(responseGetWaybill.body).to.deep.equal(payloadWaybill);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill.number}`)
        Requests.deletePayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill2.number}`)
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`);
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer2.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard2.code}`);
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`);
    });

});