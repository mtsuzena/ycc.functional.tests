import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadWaybill = require('../payloads/waybill.json');
const payloadCommodity = require('../../commodity/payloads/commodity.json');
const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadCustomer = require('../../customer/payloads/customer.json');
const payloadCustomer2 = require('../../customer/payloads/customer-2.json');

const customertBaseUrl = 'Customer';
const YardBaseUrl = 'Yard';
const commodityBaseUrl = 'Commodity';
const WaybillBaseUrl = 'Waybill';
var token = '';

describe('DELETE Waybill', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, commodityBaseUrl, payloadCommodity);
            Requests.addMultiplePayloads(token, `${YardBaseUrl}/yards`, [payloadYard, payloadYard2]);
            Requests.addMultiplePayloads(token, customertBaseUrl+"/customers", [payloadCustomer, payloadCustomer2]);
            Requests.addPayload(token, WaybillBaseUrl, payloadWaybill);
        });
    });

    it('YCC-528 - [DELETE] - Delete an existent waybill by number', () => {
        Requests.deletePayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill.number}`).should((responseDeleteWaybill) => {
            expect(responseDeleteWaybill.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`);
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer2.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard2.code}`);
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`);
    });

});