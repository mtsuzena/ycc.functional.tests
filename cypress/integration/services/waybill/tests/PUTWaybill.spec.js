import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadWaybill = require('../payloads/waybill.json');
const payloadCommodity = require('../../commodity/payloads/commodity.json');
const payloadCommodity2 = require('../../commodity/payloads/commodity-2.json');
const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadCustomer = require('../../customer/payloads/customer.json');
const payloadCustomer2 = require('../../customer/payloads/customer-2.json');

const customertBaseUrl = 'Customer';
const YardBaseUrl = 'Yard';
const commodityBaseUrl = 'Commodity';
const WaybillBaseUrl = 'Waybill';

var token = '';

describe('PUT Waybill', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, commodityBaseUrl, payloadCommodity);
            Requests.addPayload(token, commodityBaseUrl, payloadCommodity2);
            Requests.addMultiplePayloads(token, `${YardBaseUrl}/yards`, [payloadYard, payloadYard2]);
            Requests.addMultiplePayloads(token, customertBaseUrl+"/customers", [payloadCustomer, payloadCustomer2]);
            Requests.addPayload(token, WaybillBaseUrl, payloadWaybill);
        });
    });

    it('YCC-527 - [PUT] - Updates a waybill by number', () => {
        var payloadWaybillUpdated = payloadWaybill;

        payloadWaybillUpdated.expediteShipment = !payloadWaybill.expediteShipment;
        
        let yardOriginUpdated = payloadWaybill.yardCodeDestination;
        let yardDestinationUpdated = payloadWaybill.yardCodeOrigin;
        payloadWaybillUpdated.yardCodeDestination = yardDestinationUpdated;
        payloadWaybillUpdated.yardCodeOrigin = yardOriginUpdated;

        let shipperCodeUpdated = payloadWaybill.consigneeCode;
        let consigneeCodeUpdated = payloadWaybill.shipperCode;
        payloadWaybillUpdated.consigneeCode = consigneeCodeUpdated;
        payloadWaybillUpdated.shipperCode = shipperCodeUpdated;

        payloadWaybillUpdated.weight = 76100.00;

        payloadWaybillUpdated.hazmatCode = null;
        payloadWaybillUpdated.specialHandlingCode = null;
        payloadWaybillUpdated.commodityCode = payloadCommodity2.code;

        Requests.updatePayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill.number}`, payloadWaybillUpdated).should((responseUpdateWaybill) => {
            expect(responseUpdateWaybill.status).to.eq(200);
            Requests.getPayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill.number}`).should((responseGetWaybill) => {
                if(responseGetWaybill.status == 200){
                    expect(responseGetWaybill.body).to.deep.equal(payloadWaybillUpdated);
                }
            });
        });

    });

    after(() => {
        Requests.deletePayloadByCode(token, `${WaybillBaseUrl}/${payloadWaybill.number}`);
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`);
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer2.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard2.code}`);
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`);
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity2.code}`);
    });

});