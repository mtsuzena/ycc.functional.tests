import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadFleetRailcar = require('../payloads/fleet_railcar.json');
const payloadFleetLocomotive = require('../payloads/fleet_locomotive.json');
const fleetBaseUrl = 'Fleet';
var token = '';

describe('GET Fleet', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addMultiplePayloads(token, fleetBaseUrl+'/fleets', [payloadFleetRailcar, payloadFleetLocomotive]);
        });
    });

    it('YCC-422 - [GET] - Get all fleets', () => {
        Requests.getAllPayloads(token, fleetBaseUrl).should((responseGetAllFleets) => {
            expect(responseGetAllFleets.status).to.eq(200);
            expect(responseGetAllFleets.body).to.be.not.null;

            var responsePayloadRailcarFleet = '';
            var responsePayloadLocomotiveFleet = '';

            responseGetAllFleets.body.results.map(function(fleet, i){
                if(payloadFleetRailcar.code == fleet.code){
                    responsePayloadRailcarFleet = fleet;
                }
                if(payloadFleetLocomotive.code == fleet.code){
                    responsePayloadLocomotiveFleet = fleet;
                }
            });

            expect(responsePayloadRailcarFleet).to.deep.equal(payloadFleetRailcar);
            expect(responsePayloadLocomotiveFleet).to.deep.equal(payloadFleetLocomotive);
        });
    });

    it('YCC-424:[GET] - Get Railcar Fleet by code', () => {
        Requests.getPayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`).should((responseGetRailcarFleet) => {
            expect(responseGetRailcarFleet.status).to.eq(200);
            expect(responseGetRailcarFleet.body).to.be.not.null;
            expect(responseGetRailcarFleet.body).to.deep.equal(payloadFleetRailcar);
        });
    });

    it('YCC-564:[GET] - Get Locomotive Fleet by code', () => {
        Requests.getPayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`).should((responseGetLocomotiveFleet) => {
            expect(responseGetLocomotiveFleet.status).to.eq(200);
            expect(responseGetLocomotiveFleet.body).to.be.not.null;
            expect(responseGetLocomotiveFleet.body).to.deep.equal(payloadFleetLocomotive);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`);
    });

});