import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadFleetRailcar = require('../payloads/fleet_railcar.json');
const payloadFleetLocomotive = require('../payloads/fleet_locomotive.json');
const fleetBaseUrl = 'Fleet';
var token = '';

describe('POST Fleet', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-423 - [POST] - Creates a new fleet', () => {
        Requests.addPayload(token, fleetBaseUrl, payloadFleetRailcar).should((responseAddFleetRailcar) => {
            expect(responseAddFleetRailcar.status).to.eq(200);
        });
    });

    it('YCC-427 - [POST] - Creates multiple fleets', () => {
        Requests.addMultiplePayloads(token, fleetBaseUrl+'/fleets', [payloadFleetRailcar, payloadFleetLocomotive]).should((responseAddMultipleFleets) => {
            expect(responseAddMultipleFleets.status).to.eq(200);
        });
    });

    afterEach(() => {
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`);
    });

});