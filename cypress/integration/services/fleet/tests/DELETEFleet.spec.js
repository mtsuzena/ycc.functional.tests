import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadFleetRailcar = require('../payloads/fleet_railcar.json');
const payloadFleetLocomotive = require('../payloads/fleet_locomotive.json');
const fleetBaseUrl = 'Fleet';
var token = '';

describe('DELETE Fleet', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addMultiplePayloads(token, fleetBaseUrl+'/fleets', [payloadFleetRailcar, payloadFleetLocomotive]);
        });
    });

    it('YCC-563 - [DELETE] - Delete an existent locomotive fleet by code', () => {
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`).should((responseDeleteLocomotiveFLeet) => {
            expect(responseDeleteLocomotiveFLeet.status).to.eq(200);
        });
    });

    it('YCC-426 - [DELETE] - Delete an existent railcar fleet by code', () => {
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`).should((responseDeleteRailcarFLeet) => {
            expect(responseDeleteRailcarFLeet.status).to.eq(200);
        });
    });

});