import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadFleetRailcar = require('../payloads/fleet_railcar.json');
const fleetBaseUrl = 'Fleet';
var token = '';

describe('PUT Fleet', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, fleetBaseUrl, payloadFleetRailcar);
        });
    });

    it('YCC-425 - [PUT] - Updates a fleet by code', () => {
        var payloadFleetRailcarUpdated = payloadFleetRailcar;
        payloadFleetRailcarUpdated.label = "up_lab";
        payloadFleetRailcarUpdated.description = "Fleet Locomotive Description Upd Railcar";

        Requests.updatePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}`, payloadFleetRailcarUpdated).should((responseUpdateRailcarFleet) => {
            expect(responseUpdateRailcarFleet.status).to.eq(200);
            Requests.getPayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`).should((responseGetRailcarFleet) => {
                if(responseGetRailcarFleet.status==200){
                    expect(responseGetRailcarFleet.body).to.deep.equal(payloadFleetRailcarUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
    });

});