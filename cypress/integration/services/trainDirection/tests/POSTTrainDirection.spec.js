import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadTrainDirection = require('../payloads/train-direction.json');

const YardBaseUrl = 'Yard';
const trainDirectionBaseUrl = 'TrainDirection';

var token = '';

describe('POST Train Direction', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
        });
    });

    it('YCC-504:[POST] - Creates a new TrainDirection', () => {
        Requests.addPayload(token, `${trainDirectionBaseUrl}/${payloadYard.code}`, payloadTrainDirection).should((responseAddTrainDirection) => {
            expect(responseAddTrainDirection.status).to.eq(200);
        });
    });

    after(() => {
        Requests.addPayload(token, `${trainDirectionBaseUrl}/${payloadYard.code}`, []);
        Requests.deletePayloadByCode(token,`${YardBaseUrl}/${payloadYard.code}`);
    });

});