import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadTrainDirection = require('../payloads/train-direction.json');

const YardBaseUrl = 'Yard';
const trainDirectionBaseUrl = 'TrainDirection';

var token = '';

describe('GET Train Direction', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, `${trainDirectionBaseUrl}/${payloadYard.code}`, payloadTrainDirection);
        });
    });

    it('YCC-503 :: Version : 1 :: [GET] - Get list of TrainDirection by yardCode ', () => {
        Requests.getPayloadByCode(token, `${trainDirectionBaseUrl}/${payloadYard.code}`).should((responseGetTrainDirection) => {
            expect(responseGetTrainDirection.status).to.eq(200);
            expect(responseGetTrainDirection.body[0]).to.deep.equal(payloadTrainDirection[0]);
        });
    });

    after(() => {
        Requests.addPayload(token, `${trainDirectionBaseUrl}/${payloadYard.code}`, []);
        Requests.deletePayloadByCode(token,`${YardBaseUrl}/${payloadYard.code}`);
    });
    
});