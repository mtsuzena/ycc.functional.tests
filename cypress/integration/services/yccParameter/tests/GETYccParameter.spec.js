import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYccParameter = require('../payloads/ycc-parameter.json');
const payloadYccParameter2 = require('../payloads/ycc-parameter-2.json');
const yccParameterBaseUrl = 'YccParameter';

var token = '';

describe('GET YCC Parameter', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yccParameterBaseUrl, payloadYccParameter);
            Requests.addPayload(token, yccParameterBaseUrl, payloadYccParameter2);
        });
    });

    it('YCC-544 - [GET] - Get all ycc parameters', () => {
        Requests.getAllPayloads(token, yccParameterBaseUrl).should((responseGetYccParameters) => {
            expect(responseGetYccParameters.status).to.eq(200);

            var responsePayloadYccParameter = '';
            var responsePayloadYccParameter2 = '';

            responseGetYccParameters.body.map((yccParameter) => {
                if(payloadYccParameter.code == yccParameter.code){
                    responsePayloadYccParameter = yccParameter;
                }
                if(payloadYccParameter2.code == yccParameter.code){
                    responsePayloadYccParameter2 = yccParameter;
                }
            });

            expect(responsePayloadYccParameter).to.deep.equal(payloadYccParameter);
            expect(responsePayloadYccParameter2).to.deep.equal(payloadYccParameter2);
        });
    });

    it('YCC-546 - [GET] - Get ycc parameter by code', () => {
        Requests.getPayloadByCode(token, `${yccParameterBaseUrl}/${payloadYccParameter.code}`).should((responseGetYccParameter) => {
            expect(responseGetYccParameter.status).to.eq(200);
            expect(responseGetYccParameter.body).to.deep.equal(payloadYccParameter);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${yccParameterBaseUrl}/${payloadYccParameter.code}`);
        Requests.deletePayloadByCode(token, `${yccParameterBaseUrl}/${payloadYccParameter2.code}`);
    });
    
});