import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYccParameter = require('../payloads/ycc-parameter.json');
const yccParameterBaseUrl = 'YccParameter';

var token = '';

describe('PUT YCC Parameter', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yccParameterBaseUrl, payloadYccParameter);
        });
    });

    it('YCC-547 - [PUT] - Updates an existing ycc parameter by code', () => {
        var payloadYccParameterUpdated = payloadYccParameter;
        payloadYccParameterUpdated.description = "Updated";
        payloadYccParameterUpdated.type = "String";
        payloadYccParameterUpdated.value = "Updated value";

        Requests.updatePayloadByCode(token, `${yccParameterBaseUrl}/${payloadYccParameter.code}`, payloadYccParameterUpdated).should((responseUpdateYccParameter) => {
            expect(responseUpdateYccParameter.status).to.eq(200);
            Requests.getPayloadByCode(token, `${yccParameterBaseUrl}/${payloadYccParameter.code}`).should((responseGetYccParameter) => {
                if(responseGetYccParameter.status == 200) {
                    expect(responseGetYccParameter.body).to.deep.equal(payloadYccParameterUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${yccParameterBaseUrl}/${payloadYccParameter.code}`);
    });
    
});