import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYccParameter = require('../payloads/ycc-parameter.json');
const yccParameterBaseUrl = 'YccParameter';

var token = '';

describe('DELETE YCC Parameter', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yccParameterBaseUrl, payloadYccParameter);
        });
    });

    it('YCC-548 - [DELETE] - Delete an existent ycc parameter by code', () => {
        Requests.deletePayloadByCode(token, `${yccParameterBaseUrl}/${payloadYccParameter.code}`).should((responseDeleteYccParameter) => {
            expect(responseDeleteYccParameter.status).to.eq(200);
        });
    });
    
});