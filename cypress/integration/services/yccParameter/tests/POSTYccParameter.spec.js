import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYccParameter = require('../payloads/ycc-parameter.json');
const yccParameterBaseUrl = 'YccParameter';

var token = '';

describe('POST YCC Parameter', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-545 - [POST] - Creates a new ycc parameter', () => {
        Requests.addPayload(token, yccParameterBaseUrl, payloadYccParameter).should((responseAddYccParameter) => {
            expect(responseAddYccParameter.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${yccParameterBaseUrl}/${payloadYccParameter.code}`);
    });
    
});