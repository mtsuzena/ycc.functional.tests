import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../payloads/map.json');
const payloadMap2 = require('../payloads/map-2.json');

const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
const MapBaseUrl = 'Map';

var mapId = 0;

var token = '';

describe('GET Map', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
            Requests.addPayload(token, MapBaseUrl, payloadMap);
            Requests.addPayload(token, MapBaseUrl, payloadMap2);
        });
    });

    it('YCC-462 :: Version : 1 :: [GET] - List all maps', () => {
        Requests.getAllPayloads(token, MapBaseUrl).should((responseGetAllMaps) => {

            expect(responseGetAllMaps.status).to.eq(200);

            var responsePayloadMap = '';
            var responsePayloadMap2 = '';

            responseGetAllMaps.body.map((map) => {
                if(payloadMap.code == map.code){
                    responsePayloadMap = map;
                    mapId = map.id;
                }
                if(payloadMap2.code == map.code){
                    responsePayloadMap2 = map;
                }
            });

            expect(responsePayloadMap.code).to.eq(payloadMap.code);
            expect(responsePayloadMap2.code).to.eq(payloadMap2.code);
        });
    });

    it('YCC-464 :: Version : 1 :: [GET] - Get map details by id', () => {
        Requests.getPayloadByCode(token, `${MapBaseUrl}/${mapId}`).should((responseGetMapFull) => {
            expect(responseGetMapFull.status).to.eq(200);
            expect(responseGetMapFull.body.mapDesign).to.eq(payloadMap.mapDesign);
            expect(responseGetMapFull.body.code).to.eq(payloadMap.code);
        });
    });

    it('YCC-465 :: Version : 1 :: [GET] - Check if user can access the map ', () => {
        Requests.getPayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}/canAccess/PerformSwitches`).should((responseGetUserCanAccessTheMap) => {
            expect(responseGetUserCanAccessTheMap.status).to.eq(200);
            expect(responseGetUserCanAccessTheMap.body).to.eq(true);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap2.code}`);
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });

});