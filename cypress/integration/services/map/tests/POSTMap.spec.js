import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../payloads/map.json');

const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
const MapBaseUrl = 'Map';

var token = '';

describe('POST Map', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
        });
    });

    it('YCC-463 - [POST] - Creates a new map', () => {
        Requests.addPayload(token, MapBaseUrl, payloadMap).should((responseAddMap) => {
            expect(responseAddMap.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });
});