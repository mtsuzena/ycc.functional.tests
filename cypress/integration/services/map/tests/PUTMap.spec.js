import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../payloads/map.json');

const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
const MapBaseUrl = 'Map';

var token = '';

describe('PUT Map', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
            Requests.addPayload(token, MapBaseUrl, payloadMap);
        });
    });

    it('YCC-466 :: Version : 1 :: [PUT] - Updates a map by mapCode ', () => {
        
        var payloadMapUpdated = payloadMap;
        payloadMapUpdated.description = "updated";

        Requests.updatePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`, payloadMapUpdated).should((responseUpdateMap) => {
            expect(responseUpdateMap.status).to.eq(200);
            Requests.getAllPayloads(token, MapBaseUrl).should((responseGetAllMaps) => {
                responseGetAllMaps.body.map((map) => {
                    if(payloadMap.code == map.code){
                        Requests.getPayloadByCode(token, `${MapBaseUrl}/${map.id}`).should((responseGetMap) => {
                            expect(responseGetMap.body.code).to.eq(payloadMapUpdated.code);
                            expect(responseGetMap.body.description).to.eq(payloadMapUpdated.description);
                        });
                    }
                });
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });

});