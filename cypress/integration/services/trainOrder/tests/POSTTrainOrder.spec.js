import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadTrainOrder = require('../payloads/train-order.json');

const yardBaseUrl = 'Yard';
const trainOrderBaseUrl = 'TrainOrder';

var token = '';

describe('POST Train Order', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, yardBaseUrl, payloadYard2);
        });
    });

    it('YCC-506 :: Version : 1 :: [POST] - Creates a new train order', () => {
        Requests.addPayload(token, trainOrderBaseUrl, payloadTrainOrder).should((responseAddTrainOrder) => {
            expect(responseAddTrainOrder.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token,`${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard2.code}`);
    });

});