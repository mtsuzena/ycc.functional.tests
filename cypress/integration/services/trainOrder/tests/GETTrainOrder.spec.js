import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadTrainOrder = require('../payloads/train-order.json');
const payloadTrainOrder2 = require('../payloads/train-order-2.json');

const yardBaseUrl = 'Yard';
const trainOrderBaseUrl = 'TrainOrder';

var token = '';

describe('GET Train Order', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, yardBaseUrl, payloadYard2);
            Requests.addPayload(token, trainOrderBaseUrl, payloadTrainOrder);
            Requests.addPayload(token, trainOrderBaseUrl, payloadTrainOrder2);
        });
    });

    it('YCC-505 :: Version : 1 :: [GET] - Get all train orders', () => {
        Requests.getAllPayloads(token, trainOrderBaseUrl).should((responseGetAllTrainOrders) => {
            expect(responseGetAllTrainOrders.status).to.eq(200);

            var responsePayloadTrainOrder = '';
            var responsePayloadTrainOrder2 = '';

            responseGetAllTrainOrders.body.results.map((trainOrder) => {
                if(payloadTrainOrder.trainId == trainOrder.trainId){
                    responsePayloadTrainOrder = trainOrder;
                }
                if(payloadTrainOrder2.trainId == trainOrder.trainId){
                    responsePayloadTrainOrder2 = trainOrder;
                }
            });

            expect(responsePayloadTrainOrder).to.deep.equal(payloadTrainOrder);
            expect(responsePayloadTrainOrder2).to.deep.equal(payloadTrainOrder2);
        });
    });

    it('YCC-507 :: Version : 1 :: [GET] - Get train order by trainId', () => {
        Requests.getPayloadByCode(token, `${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`).should((responseGetTrainOrder) => {
            expect(responseGetTrainOrder.status).to.eq(200);
            expect(responseGetTrainOrder.body).to.deep.equal(payloadTrainOrder);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token,`${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`);
        Requests.deletePayloadByCode(token,`${trainOrderBaseUrl}/${payloadTrainOrder2.trainId}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard2.code}`);
    });

});