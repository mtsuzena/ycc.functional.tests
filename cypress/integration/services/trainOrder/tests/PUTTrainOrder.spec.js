import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadTrainOrder = require('../payloads/train-order.json');

const yardBaseUrl = 'Yard';
const trainOrderBaseUrl = 'TrainOrder';

var token = '';

describe('PUT Train Order', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, yardBaseUrl, payloadYard2);
            Requests.addPayload(token, trainOrderBaseUrl, payloadTrainOrder);
        });
    });

    it('YCC-508 :: Version : 1 :: [PUT] - Updates a train order by trainId', () => {
        var payloadTrainOrderUpdated = payloadTrainOrder;
        payloadTrainOrderUpdated.etd = "2021-04-30T03:30:12Z";
        payloadTrainOrderUpdated.totalLength = 56256.5,
        payloadTrainOrderUpdated.totalGrossWeight = 14.5,
        payloadTrainOrderUpdated.yardCodeDestination = payloadTrainOrder.yardCodeOrigin;
        payloadTrainOrderUpdated.yardCodeOrigin = payloadTrainOrder.yardCodeDestination;

        Requests.updatePayloadByCode(token, `${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`, payloadTrainOrderUpdated).should((responseUpdateTrainOrder) => {
            expect(responseUpdateTrainOrder.status).to.eq(200);
            Requests.getPayloadByCode(token, `${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`).should((responseGetTrainOrder) => {
                if(responseGetTrainOrder.status == 200){
                    expect(responseGetTrainOrder.body).to.deep.equal(payloadTrainOrderUpdated)
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token,`${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard2.code}`);
    });

});