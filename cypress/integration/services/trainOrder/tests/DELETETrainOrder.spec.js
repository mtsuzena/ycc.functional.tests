import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadTrainOrder = require('../payloads/train-order.json');

const yardBaseUrl = 'Yard';
const trainOrderBaseUrl = 'TrainOrder';

var token = '';

describe('DELETE Train Order', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, yardBaseUrl, payloadYard2);
            Requests.addPayload(token, trainOrderBaseUrl, payloadTrainOrder);
        });
    });

    it('YCC-509 :: Version : 1 :: [DELETE] - Delete an existent train order by trainId', () => {
        Requests.deletePayloadByCode(token,`${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`).should((responseDeleteTrainOrder) => {
            expect(responseDeleteTrainOrder.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard2.code}`);
    });

});