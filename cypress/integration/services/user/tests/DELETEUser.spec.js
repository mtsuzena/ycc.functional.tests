import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadUser = require('../payloads/user.json');
const UserBaseUrl = 'User';
var token = '';

describe('DELETE User', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, UserBaseUrl, payloadUser);
        });
    });

    it('YCC-516:[DELETE] - Delete an existent user by username', () => {
        Requests.deletePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}`).should((responseDeleteUser) => {
            expect(responseDeleteUser.status).to.eq(200);
        });
    });

});