import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadUser = require('../payloads/user.json');
const payloadLocalizationProfile = require('../../localizationProfile/payloads/localization-profile.json');
const payloadUserProfile = require('../payloads/user-profile.json');
const LocalizationProfileBaseUrl = 'LocalizationProfile';
const UserBaseUrl = 'User';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../../map/payloads/map.json');
const payloadPerformSwitchesUserMapPermission = require('../payloads/performSwitches-user-map-permission.json');
const payloadViewOnlyUserMapPermission = require('../payloads/viewOnly-user-map-permission.json');

const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
const MapBaseUrl = 'Map';

var token = '';

describe('PUT User', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocalizationProfileBaseUrl, payloadLocalizationProfile);
            Requests.addPayload(token, UserBaseUrl, payloadUser);
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
            Requests.addPayload(token, MapBaseUrl, payloadMap);
        });
    });

    it('YCC-515:[PUT] - Updates an user by username', () => {
        var payloadUserUpdated = payloadUser;
        payloadUserUpdated.fullName = "Updated Fullname";
        payloadUserUpdated.password = "jasdhjasd";

        Requests.updatePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}`, payloadUserUpdated).should((responsePutUser) => {
            expect(responsePutUser.status).to.eq(200);
            Requests.getPayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}`).should((responseGetUserByUsername) => {
                if(responseGetUserByUsername.status == 200){
                    expect(responseGetUserByUsername.body.fullName).to.eq(payloadUserUpdated.fullName);
                }
            });
        });
    });

    it('YCC-518 - [PUT] - Update user profile by username', () => {
        Requests.updatePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}/profile`, payloadUserProfile).should((responseUpdateUserProfile) => {
            expect(responseUpdateUserProfile.status).to.eq(200);
        });
    });

    it('YCC-559 :: Version : 1 :: [PUT] - Update user permissions - Perform Switches', () => {
        Requests.updatePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}/mappermission/${payloadMap.code}`, payloadViewOnlyUserMapPermission).should((responseUpdateUserMapPermission) => {
            expect(responseUpdateUserMapPermission.status).to.eq(200);
            Requests.getPayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}/withProfile`).should((responseGetUserWithProfileByUserName) => {
                responseGetUserWithProfileByUserName.body.mapPermissions.map((mapPermission) => {
                    expect(mapPermission.mapCode).to.eq(payloadMap.code);
                    expect(mapPermission.permission).to.eq("ViewOnly");
                });
            });
        });
    });

    it('YCC-519 :: Version : 1 :: [PUT] - Update user permissions - View Only ', () => {
        Requests.updatePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}/mappermission/${payloadMap.code}`, payloadPerformSwitchesUserMapPermission).should((responseUpdateUserMapPermission) => {
            expect(responseUpdateUserMapPermission.status).to.eq(200);
            Requests.getPayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}/withProfile`).should((responseGetUserWithProfileByUserName) => {
                responseGetUserWithProfileByUserName.body.mapPermissions.map((mapPermission) => {
                    expect(mapPermission.mapCode).to.eq(payloadMap.code);
                    expect(mapPermission.permission).to.eq("PerformSwitches");
                });
            });
        });
    });

    it('YCC-520 :: Version : 1 :: [DELETE] - Delete an user map permission ', () => {
        Requests.deletePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}/mappermission/${payloadMap.code}`).should((responseDeleteMapPermission) => {
            expect(responseDeleteMapPermission.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}`);
        Requests.deletePayloadByCode(token, `${LocalizationProfileBaseUrl}/${payloadLocalizationProfile.name}`);
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });

});