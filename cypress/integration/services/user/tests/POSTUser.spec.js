import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadUser = require('../../user/payloads/user.json');
const payloadUser2 = require('../../user/payloads/user-2.json');
const UserBaseUrl = 'User';
var token = '';

describe('POST User', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-511:[POST] - Creates an user', () => {
        Requests.addPayload(token, UserBaseUrl, payloadUser).should((responseAddUser) => {
            expect(responseAddUser.status).to.eq(200);
        });
    });

    it('YCC-517 - [POST] - Creates multiple users', () => {
        Requests.addMultiplePayloads(token, UserBaseUrl+"/users", [payloadUser, payloadUser2]).should((responseAddUsers) => {
            expect(responseAddUsers.status).to.eq(200);
        });
    });

    afterEach(() => {
        Requests.deletePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}`);
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${UserBaseUrl}/${payloadUser2.userName}`);
    });

});