import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadUser = require('../payloads/user.json');
const payloadUser2 = require('../payloads/user-2.json');
const payloadUserProfile = require('../payloads/user-profile.json');
const payloadLocalizationProfile = require('../../localizationProfile/payloads/localization-profile.json');

const UserBaseUrl = 'User';
const LocalizationProfileBaseUrl = 'LocalizationProfile';

var token = '';

describe('GET User', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocalizationProfileBaseUrl, payloadLocalizationProfile);
            Requests.addPayload(token, UserBaseUrl, payloadUser).should((response) => {
                Requests.updatePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}/profile`, payloadUserProfile);
            })
            Requests.addPayload(token, UserBaseUrl, payloadUser2).should((response) => {
                Requests.updatePayloadByCode(token, `${UserBaseUrl}/${payloadUser2.userName}/profile`, payloadUserProfile);
            })
        });
    });

    it('YCC-510:[GET] - Get all users', () => {
        Requests.getAllPayloads(token, UserBaseUrl).should((responseGetAllUsers) => {
            expect(responseGetAllUsers.status).to.eq(200);
            expect(responseGetAllUsers.body).to.be.not.null;

            var responsePayloadUser = '';
            var responsePayloadUser2 = '';

            responseGetAllUsers.body.results.map(function(user, i){
                if(payloadUser.userName == user.userName){
                    responsePayloadUser = user;
                }

                if(payloadUser2.userName == user.userName){
                    responsePayloadUser2 = user;
                }
            });

            expect(responsePayloadUser.userName).to.eq(payloadUser.userName);
            expect(responsePayloadUser.fullName).to.eq(payloadUser.fullName);

            expect(responsePayloadUser2.userName).to.eq(payloadUser2.userName);
            expect(responsePayloadUser2.fullName).to.eq(payloadUser2.fullName);
        });
    });

    it('YCC-514 - [GET] - Get user by username', () => {
        Requests.getPayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}`).should((responseGetUserByUsername) => {
            expect(responseGetUserByUsername.status).to.eq(200);
            expect(responseGetUserByUsername.body).to.be.not.null;
            expect(responseGetUserByUsername.body.userName).to.eq(payloadUser.userName);
            expect(responseGetUserByUsername.body.fullName).to.eq(payloadUser.fullName);
        });
    });

    it('YCC-513 - [GET] - Get user with profile by username', () => {
        Requests.getPayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}/withProfile`).should((responseGetUserWithProfileByCode) => {
            expect(responseGetUserWithProfileByCode.status).to.eq(200);
            expect(responseGetUserWithProfileByCode.body).to.be.not.null;
            expect(responseGetUserWithProfileByCode.body.fullName).to.eq(payloadUser.fullName);
            expect(responseGetUserWithProfileByCode.body.username).to.eq(payloadUser.userName);
            expect(responseGetUserWithProfileByCode.body.localizationProfileCode).to.eq(payloadLocalizationProfile.name);
        });
    });

    it('YCC-512 - [GET] - Get all users with profile', () => {
        Requests.getPayloadByCode(token, `${UserBaseUrl}/all/withProfile`).should((responseGetAllUsersWithProfile) => {
            expect(responseGetAllUsersWithProfile.status).to.eq(200);
            expect(responseGetAllUsersWithProfile.body).to.be.not.null;

            var responsePayloadUser = '';
            var responsePayloadUser2 = '';

            responseGetAllUsersWithProfile.body.map(function(user, i){
                if(payloadUser.userName == user.username){
                    responsePayloadUser = user;
                }

                if(payloadUser2.userName == user.username){
                    responsePayloadUser2 = user;
                }
            });

            expect(responsePayloadUser.fullName).to.eq(payloadUser.fullName);
            expect(responsePayloadUser.username).to.eq(payloadUser.userName);
            expect(responsePayloadUser.localizationProfileCode).to.eq(payloadLocalizationProfile.name);

            expect(responsePayloadUser2.fullName).to.eq(payloadUser2.fullName);
            expect(responsePayloadUser2.username).to.eq(payloadUser2.userName);
            expect(responsePayloadUser2.localizationProfileCode).to.eq(payloadLocalizationProfile.name);

        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${UserBaseUrl}/${payloadUser.userName}`);
        Requests.deletePayloadByCode(token, `${UserBaseUrl}/${payloadUser2.userName}`);
        Requests.deletePayloadByCode(token, `${LocalizationProfileBaseUrl}/${payloadLocalizationProfile.name}`);
    });

});