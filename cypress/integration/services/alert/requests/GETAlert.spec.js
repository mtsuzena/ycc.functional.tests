import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../../map/payloads/map.json');
const payloadAlertEngineWithoutSwitchWorder = require('../payloads/alert-engine-without-switch-order.json');
const payloadAlertTrainEvent = require('../payloads/alert-train-event.json');

const gaugeBaseUrl = 'Gauge';
const yardBaseUrl = 'Yard';
const trackBaseUrl = 'Track';
const mapBaseUrl = 'Map';
const alertBaseUrl = 'Alert';

var token = '';
var alertId = 1;

describe('GET Alert', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, gaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, trackBaseUrl, payloadTrack);
            Requests.addPayload(token, mapBaseUrl, payloadMap);
            Requests.addPayload(token, alertBaseUrl, payloadAlertEngineWithoutSwitchWorder);
            Requests.addPayload(token, alertBaseUrl, payloadAlertTrainEvent);
        });
    });

    it('YCC-391 :: Version : 1 :: [GET] - Get all alerts', () => {
        var responsePayloadAlertEngineWithoutSwitchWorder = '';
        var responsePayloadAlertTrainEvent = '';

        Requests.getAllPayloads(token, alertBaseUrl).should((responseGetAllAlerts) => {

            expect(responseGetAllAlerts.status).to.eq(200);

            responseGetAllAlerts.body.map((alert) => {
                if(payloadAlertEngineWithoutSwitchWorder.description == alert.description){
                    responsePayloadAlertEngineWithoutSwitchWorder = alert;
                    alertId = alert.id;
                }
                if(payloadAlertTrainEvent.description == alert.description){
                    responsePayloadAlertTrainEvent = alert;
                }
            });

            expect(responsePayloadAlertEngineWithoutSwitchWorder.alertType).to.eq(payloadAlertEngineWithoutSwitchWorder.alertType);
            expect(responsePayloadAlertEngineWithoutSwitchWorder.description).to.eq(payloadAlertEngineWithoutSwitchWorder.description);
            expect(responsePayloadAlertEngineWithoutSwitchWorder.isOn).to.eq(payloadAlertEngineWithoutSwitchWorder.isOn);

            expect(responsePayloadAlertTrainEvent.alertType).to.eq(payloadAlertTrainEvent.alertType);
            expect(responsePayloadAlertTrainEvent.description).to.eq(payloadAlertTrainEvent.description);
            expect(responsePayloadAlertTrainEvent.isOn).to.eq(payloadAlertTrainEvent.isOn);
        });
    });

    it('YCC-394 :: Version : 1 :: [GET] - Get the alerts by Id', () => {
        Requests.getPayloadByCode(token, `${alertBaseUrl}/${alertId}`).should((responseGetAlert) => {
            expect(responseGetAlert.status).to.eq(200);
            expect(responseGetAlert.body.alertType).to.eq(payloadAlertEngineWithoutSwitchWorder.alertType);
            expect(responseGetAlert.body.description).to.eq(payloadAlertEngineWithoutSwitchWorder.description);
            expect(responseGetAlert.body.isOn).to.eq(payloadAlertEngineWithoutSwitchWorder.isOn);
        });
    });

    it('YCC-393 :: Version : 1 :: [GET] - Get the alerts by map code', () => {
        Requests.getPayloadByCode(token, `${alertBaseUrl}/${payloadMap.code}/byMap`).should((responseGetAlertByMap) => {
            expect(responseGetAlertByMap.status).to.eq(200);

            var responsePayloadAlertEngineWithoutSwitchWorder = '';
            var responsePayloadAlertTrainEvent = '';

            responseGetAlertByMap.body.map((alert) => {
                if(payloadAlertEngineWithoutSwitchWorder.description == alert.description){
                    responsePayloadAlertEngineWithoutSwitchWorder = alert;
                }
                if(payloadAlertTrainEvent.description == alert.description){
                    responsePayloadAlertTrainEvent = alert;
                }
            });

            expect(responsePayloadAlertEngineWithoutSwitchWorder.alertType).to.eq(payloadAlertEngineWithoutSwitchWorder.alertType);
            expect(responsePayloadAlertEngineWithoutSwitchWorder.description).to.eq(payloadAlertEngineWithoutSwitchWorder.description);
            expect(responsePayloadAlertEngineWithoutSwitchWorder.isOn).to.eq(payloadAlertEngineWithoutSwitchWorder.isOn);

            expect(responsePayloadAlertTrainEvent.alertType).to.eq(payloadAlertTrainEvent.alertType);
            expect(responsePayloadAlertTrainEvent.description).to.eq(payloadAlertTrainEvent.description);
            expect(responsePayloadAlertTrainEvent.isOn).to.eq(payloadAlertTrainEvent.isOn);
        });
    });

    after(() => {
        Requests.getAllPayloads(token, alertBaseUrl).should((getAllAlerts) => {
            getAllAlerts.body.forEach((alert) => {
                Requests.deletePayloadByCode(token, `${alertBaseUrl}/${alert.id}`);
            });
        });
        Requests.deletePayloadByCode(token, `${alertBaseUrl}/${payloadAlertEngineWithoutSwitchWorder}`)
        Requests.deletePayloadByCode(token, `${mapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${trackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${gaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${yardBaseUrl}/${payloadYard.code}`);
    });
});