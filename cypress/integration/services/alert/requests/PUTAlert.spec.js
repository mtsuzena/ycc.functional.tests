import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../../map/payloads/map.json');
const payloadAlertEngineWithoutSwitchWorder = require('../payloads/alert-engine-without-switch-order.json');

const gaugeBaseUrl = 'Gauge';
const yardBaseUrl = 'Yard';
const trackBaseUrl = 'Track';
const mapBaseUrl = 'Map';
const alertBaseUrl = 'Alert';

var token = '';

describe('PUT Alert', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, gaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, trackBaseUrl, payloadTrack);
            Requests.addPayload(token, mapBaseUrl, payloadMap);
            Requests.addPayload(token, alertBaseUrl, payloadAlertEngineWithoutSwitchWorder);
        });
    });

    it('YCC-395 :: Version : 1 :: [PUT] - Updates an alert - EngineWithoutSwitchOrder', () => {
        Requests.getAllPayloads(token, alertBaseUrl).should((responseGetAllAlerts) => {
            responseGetAllAlerts.body.map((alert) => {
                if(payloadAlertEngineWithoutSwitchWorder.description == alert.description){

                    var payloadAlertEngineWithoutSwitchWorderUpdated = payloadAlertEngineWithoutSwitchWorder;
                    payloadAlertEngineWithoutSwitchWorderUpdated.description = "Updated";
                    payloadAlertEngineWithoutSwitchWorderUpdated.label = "Uppdated";
                    payloadAlertEngineWithoutSwitchWorderUpdated.isOn = false;

                    Requests.updatePayloadByCode(token, `${alertBaseUrl}/${alert.id}`, payloadAlertEngineWithoutSwitchWorderUpdated).should((responseUpdateAlert) => {
                        expect(responseUpdateAlert.status).to.eq(200);
                        Requests.getPayloadByCode(token, `${alertBaseUrl}/${alert.id}`).should((responseUpdateAlert) => {
                            if(responseUpdateAlert.status == 200){
                                expect(responseUpdateAlert.body.description).to.eq(payloadAlertEngineWithoutSwitchWorderUpdated.description);
                            }
                        });
                    });
                }
            });
        });    
    });

    after(() => {
        Requests.getAllPayloads(token, alertBaseUrl).should((getAllAlerts) => {
            getAllAlerts.body.forEach((alert) => {
                Requests.deletePayloadByCode(token, `${alertBaseUrl}/${alert.id}`);
            });
        });
        Requests.deletePayloadByCode(token, `${alertBaseUrl}/${payloadAlertEngineWithoutSwitchWorder}`)
        Requests.deletePayloadByCode(token, `${mapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${trackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${gaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${yardBaseUrl}/${payloadYard.code}`);
    });
});