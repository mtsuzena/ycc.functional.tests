import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../../map/payloads/map.json');
const payloadAlertEngineWithoutSwitchWorder = require('../payloads/alert-engine-without-switch-order.json');

const gaugeBaseUrl = 'Gauge';
const yardBaseUrl = 'Yard';
const trackBaseUrl = 'Track';
const mapBaseUrl = 'Map';
const alertBaseUrl = 'Alert';

var token = '';

describe('POST Alert', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, gaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, trackBaseUrl, payloadTrack);
            Requests.addPayload(token, mapBaseUrl, payloadMap);
        });
    });

    it('YCC-392:[POST] - Creates a new alert - EngineWithoutSwitchOrder', () => {
        Requests.addPayload(token, alertBaseUrl, payloadAlertEngineWithoutSwitchWorder).should((responseGetAlert) => {
            expect(responseGetAlert.status).to.eq(200);
        });
    });

    after(() => {
        Requests.getAllPayloads(token, alertBaseUrl).should((getAllAlerts) => {
            getAllAlerts.body.forEach((alert) => {
                Requests.deletePayloadByCode(token, `${alertBaseUrl}/${alert.id}`);
            });
        });
        Requests.deletePayloadByCode(token, `${alertBaseUrl}/${payloadAlertEngineWithoutSwitchWorder}`)
        Requests.deletePayloadByCode(token, `${mapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${trackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${gaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${yardBaseUrl}/${payloadYard.code}`);
    });
});