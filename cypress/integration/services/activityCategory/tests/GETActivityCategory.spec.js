import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const activiTyCategoryBaseUrl = 'ActivityCategory';

var token = '';

describe('GET Activity Category', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-390 :: Version : 1 :: [GET] - Get all activity categories ', () => {
        Requests.getAllPayloads(token, activiTyCategoryBaseUrl).should((responseGetAllActivityCategories) => {
            expect(responseGetAllActivityCategories.status).to.eq(200);
            responseGetAllActivityCategories.body.map((activityCategory) => {
                if(activityCategory.code == "REA"){
                    expect(activityCategory.code).to.eq("REA");
                    expect(activityCategory.type).to.eq("YardRearrangement");
                }
                if(activityCategory.code == "END"){
                    expect(activityCategory.code).to.eq("END");
                    expect(activityCategory.type).to.eq("SpotPull");
                }
            });
        });
    });

});