import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotiveType = require('../payloads/locomotive-type.json');
const LocomotiveTypeBaseUrl = 'LocomotiveType';

var token = '';

describe('PUT Locomotive Type', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
        });
    });

    it('YCC-458 - [PUT] - Updates a locomotive type by code', () => {
        var payloadLocomotiveTypeUpdated = payloadLocomotiveType;
        payloadLocomotiveTypeUpdated.description = "CYPRESS_LOCOMOTIVE_TYPE_DESCRIPTION_UPDATED";

        Requests.updatePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`, payloadLocomotiveTypeUpdated)
        .should((responseUpdateLocomotiveType) => {
            expect(responseUpdateLocomotiveType.status).to.eq(200);
            Requests.getPayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`).should((responseGetLocomotiveType) => {
                if(responseGetLocomotiveType.status = 200){
                    expect(responseGetLocomotiveType.body).to.deep.equal(payloadLocomotiveTypeUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});