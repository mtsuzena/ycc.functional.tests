import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotiveType = require('../payloads/locomotive-type.json');
const payloadLocomotiveType2 = require('../payloads/locomotive-type-2.json');
var token = '';
const LocomotiveTypeBaseUrl = 'LocomotiveType'

describe('GET Locomotive Type', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType2);
        });
    });

    it('YCC-455 - [GET] - Get all locomotive types', () => {
        Requests.getAllPayloads(token, LocomotiveTypeBaseUrl).should((responseGetAllLocomotiveTypes) => {
            expect(responseGetAllLocomotiveTypes.status).to.eq(200);

            var responsePayloadLocomotiveType = '';
            var responsePayloadLocomotiveType2 = '';

            responseGetAllLocomotiveTypes.body.results.map((locomotiveType, i) => {
                if(payloadLocomotiveType.code == locomotiveType.code){
                    responsePayloadLocomotiveType = locomotiveType;
                }
                if(payloadLocomotiveType2.code == locomotiveType.code){
                    responsePayloadLocomotiveType2 = locomotiveType;
                }
            });

            expect(responsePayloadLocomotiveType).to.deep.equal(payloadLocomotiveType);
            expect(responsePayloadLocomotiveType2).to.deep.equal(payloadLocomotiveType2);
        });
    });

    it('YCC-457 - [GET] - Get locomotive type by code', () => {
        Requests.getPayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`).should((responseGetLocomotiveType) => {
            expect(responseGetLocomotiveType.status).to.eq(200);
            expect(responseGetLocomotiveType.body).to.deep.equal(payloadLocomotiveType);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType2.code}`);
    });

});