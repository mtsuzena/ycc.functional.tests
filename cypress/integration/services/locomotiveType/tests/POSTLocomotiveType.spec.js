import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotiveType = require('../payloads/locomotive-type.json');
var token = '';
const LocomotiveTypeBaseUrl = 'LocomotiveType'

describe('POST Locomotive Type', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-456 - [POST] - Creates a new locomotive type', () => {
        Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType).should((responseAddLocomotiveType) => {
            expect(responseAddLocomotiveType.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});