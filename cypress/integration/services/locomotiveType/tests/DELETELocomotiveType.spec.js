import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotiveType = require('../payloads/locomotive-type.json');
var token = '';
const LocomotiveTypeBaseUrl = 'LocomotiveType'

describe('DELETE Locomotive Type', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
        });
    });

    it('YCC-459 - [DELETE] - Delete an existent locomotive type by code', () => {
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`).should((responseDeleteLocomotiveType) => {
            expect(responseDeleteLocomotiveType.status).to.eq(200);
        });
    });

});