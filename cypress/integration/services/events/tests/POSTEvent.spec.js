import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarEventResponsible = require('../payloads/railcar-event-type-responsible.json');
const payloadRailcarEventAvailability = require('../payloads/railcar-event-type-availability.json');
const payloadRailcarEventLocation = require('../payloads/railcar-event-type-location.json');
const payloadRailcarEventState = require('../payloads/railcar-event-type-state.json');
const payloadRailcarEventExchange = require('../payloads/railcar-event-type-exchange.json');
const payloadRailcarEventCargo = require('../payloads/railcar-event-type-cargo.json');
const payloadRailcarEventCondition = require('../payloads/railcar-event-type-condition.json');

const payloadLocomotiveEventResponsible = require('../payloads/locomotive-event-type-responsible.json');
const payloadLocomotiveEventAvailability = require('../payloads/locomotive-event-type-availability.json');
const payloadLocomotiveEventLocation = require('../payloads/locomotive-event-type-location.json');
const payloadLocomotiveEventState = require('../payloads/locomotive-event-type-state.json');
const payloadLocomotiveEventExchange = require('../payloads/locomotive-event-type-exchange.json');
const payloadLocomotiveEventCargo = require('../payloads/locomotive-event-type-cargo.json');
const payloadLocomotiveEventCondition = require('../payloads/locomotive-event-type-condition.json');

const eventBaseUrl = 'Events';

var token = '';

describe('POST Event', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-417 :: Version : 1 :: [POST] - Creates a new event - [Type: Responsible] [vehicleType: Railcar]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadRailcarEventResponsible).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-568 :: Version : 1 :: [POST] - Creates a new event - [Type: Availability] [vehicleType: Railcar]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadRailcarEventAvailability).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-569 :: Version : 1 :: [POST] - Creates a new event - [Type: Location] [vehicleType: Railcar]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadRailcarEventLocation).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-570 :: Version : 1 :: [POST] - Creates a new event - [Type: State] [vehicleType: Railcar]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadRailcarEventState).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-571 :: Version : 1 :: [POST] - Creates a new event - [Type: Exchange] [vehicleType: Railcar]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadRailcarEventExchange).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-572 :: Version : 1 :: [POST] - Creates a new event - [Type: Cargo] [vehicleType: Railcar]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadRailcarEventCargo).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-573 :: Version : 1 :: [POST] - Creates a new event - [Type: Condition] [vehicleType: Railcar]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadRailcarEventCondition).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-574 :: Version : 1 :: [POST] - Creates a new event - [Type: Responsible] [vehicleType: Locomotive]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventResponsible).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });
    
    it('YCC-575 :: Version : 1 :: [POST] - Creates a new event - [Type: Availability] [vehicleType: Locomotive]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventAvailability).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-576 :: Version : 1 :: [POST] - Creates a new event - [Type: Location] [vehicleType: Locomotive]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventLocation).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-577 :: Version : 1 :: [POST] - Creates a new event - [Type: State] [vehicleType: Locomotive]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventState).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-578 :: Version : 1 :: [POST] - Creates a new event - [Type: Exchange] [vehicleType: Locomotive]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventExchange).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-579 :: Version : 1 :: [POST] - Creates a new event - [Type: Cargo] [vehicleType: Locomotive]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventCargo).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    it('YCC-580 :: Version : 1 :: [POST] - Creates a new event - [Type: Condition] [vehicleType: Locomotive]', () => {
        Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventCondition).should((responseAddEvent) => {
            expect(responseAddEvent.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventResponsible.code}?eventType=${payloadRailcarEventResponsible.type}&vehicleType=${payloadRailcarEventResponsible.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventAvailability.code}?eventType=${payloadRailcarEventAvailability.type}&vehicleType=${payloadRailcarEventAvailability.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventLocation.code}?eventType=${payloadRailcarEventLocation.type}&vehicleType=${payloadRailcarEventLocation.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventState.code}?eventType=${payloadRailcarEventState.type}&vehicleType=${payloadRailcarEventState.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventExchange.code}?eventType=${payloadRailcarEventExchange.type}&vehicleType=${payloadRailcarEventExchange.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventCargo.code}?eventType=${payloadRailcarEventCargo.type}&vehicleType=${payloadRailcarEventCargo.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventCondition.code}?eventType=${payloadRailcarEventCondition.type}&vehicleType=${payloadRailcarEventCondition.vehicleType}`);

        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventResponsible.code}?eventType=${payloadLocomotiveEventResponsible.type}&vehicleType=${payloadLocomotiveEventResponsible.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventAvailability.code}?eventType=${payloadLocomotiveEventAvailability.type}&vehicleType=${payloadLocomotiveEventAvailability.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventLocation.code}?eventType=${payloadLocomotiveEventLocation.type}&vehicleType=${payloadLocomotiveEventLocation.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventState.code}?eventType=${payloadLocomotiveEventState.type}&vehicleType=${payloadLocomotiveEventState.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventExchange.code}?eventType=${payloadLocomotiveEventExchange.type}&vehicleType=${payloadLocomotiveEventExchange.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventCargo.code}?eventType=${payloadLocomotiveEventCargo.type}&vehicleType=${payloadLocomotiveEventCargo.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventCondition.code}?eventType=${payloadLocomotiveEventCondition.type}&vehicleType=${payloadLocomotiveEventCondition.vehicleType}`);
    });

});