import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarEventResponsible = require('../payloads/railcar-event-type-responsible.json');
const eventBaseUrl = 'Events';

var token = '';

describe('DELETE Event', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventResponsible);
        });
    });

    it('YCC-420 :: Version : 1 :: [DELETE] - Delete an existent event by code - [Type: Responsible] [vehicleType: Railcar]', () => {
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventResponsible.code}?eventType=${payloadRailcarEventResponsible.type}&vehicleType=${payloadRailcarEventResponsible.vehicleType}`)
            .should((responseDeleteEvent) => {
                expect(responseDeleteEvent.status).to.eq(200);
            });
    });
  
});