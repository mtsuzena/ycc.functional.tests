import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarEventResponsible = require('../payloads/railcar-event-type-responsible.json');
const payloadRailcarEventAvailability = require('../payloads/railcar-event-type-availability.json');
const payloadRailcarEventLocation = require('../payloads/railcar-event-type-location.json');
const payloadRailcarEventState = require('../payloads/railcar-event-type-state.json');
const payloadRailcarEventExchange = require('../payloads/railcar-event-type-exchange.json');
const payloadRailcarEventCargo = require('../payloads/railcar-event-type-cargo.json');
const payloadRailcarEventCondition = require('../payloads/railcar-event-type-condition.json');

const payloadLocomotiveEventResponsible = require('../payloads/locomotive-event-type-responsible.json');
const payloadLocomotiveEventAvailability = require('../payloads/locomotive-event-type-availability.json');
const payloadLocomotiveEventLocation = require('../payloads/locomotive-event-type-location.json');
const payloadLocomotiveEventState = require('../payloads/locomotive-event-type-state.json');
const payloadLocomotiveEventExchange = require('../payloads/locomotive-event-type-exchange.json');
const payloadLocomotiveEventCargo = require('../payloads/locomotive-event-type-cargo.json');
const payloadLocomotiveEventCondition = require('../payloads/locomotive-event-type-condition.json');

const eventBaseUrl = 'Events';

var token = '';

describe('GET Event', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventResponsible);
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventAvailability);
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventLocation);
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventState);
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventExchange);
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventCargo);
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventCondition);
            Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventResponsible);
            Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventAvailability);
            Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventLocation);
            Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventState);
            Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventExchange);
            Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventCargo);
            Requests.addPayload(token, eventBaseUrl, payloadLocomotiveEventCondition);
        });
    });

    it('YCC-416:[GET] - Get all events', () => {
        Requests.getAllPayloads(token, eventBaseUrl).should((responseGetEvents) => {
            expect(responseGetEvents.status).to.eq(200);

            var responsePayloadRailcarEventResponsible = '';
            var responsePayloadLocomotiveEventResponsible = '';

            responseGetEvents.body.results.map((event) => {
                if(payloadRailcarEventResponsible.code == event.code){
                    responsePayloadRailcarEventResponsible = event;
                }
                if(payloadLocomotiveEventResponsible.code == event.code){
                    responsePayloadLocomotiveEventResponsible = event;
                }
            });

            expect(responsePayloadRailcarEventResponsible).to.deep.equal(payloadRailcarEventResponsible);
            expect(responsePayloadLocomotiveEventResponsible).to.deep.equal(payloadLocomotiveEventResponsible);
        });
    });

    it('YCC-588:[GET] - Get event by code - [Type: Responsible] [vehicleType: Railcar]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventResponsible.code}?eventType=${payloadRailcarEventResponsible.type}&vehicleType=${payloadRailcarEventResponsible.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadRailcarEventResponsible);
            });
    });

    it('YCC-589:[GET] - Get event by code - [Type: Availability] [vehicleType: Railcar]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventAvailability.code}?eventType=${payloadRailcarEventAvailability.type}&vehicleType=${payloadRailcarEventAvailability.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadRailcarEventAvailability);
            });
    });

    it('YCC-587:[GET] - Get event by code - [Type: Location] [vehicleType: Railcar]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventLocation.code}?eventType=${payloadRailcarEventLocation.type}&vehicleType=${payloadRailcarEventLocation.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadRailcarEventLocation);
            });
    });

    it('YCC-590:[GET] - Get event by code - [Type: State] [vehicleType: Railcar]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventState.code}?eventType=${payloadRailcarEventState.type}&vehicleType=${payloadRailcarEventState.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadRailcarEventState);
            });
    });

    it('YCC-591:[GET] - Get event by code - [Type: Exchange] [vehicleType: Railcar]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventExchange.code}?eventType=${payloadRailcarEventExchange.type}&vehicleType=${payloadRailcarEventExchange.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadRailcarEventExchange);
            });
    });

    it('YCC-592:[GET] - Get event by code - [Type: Cargo] [vehicleType: Railcar]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventCargo.code}?eventType=${payloadRailcarEventCargo.type}&vehicleType=${payloadRailcarEventCargo.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadRailcarEventCargo);
            });
    });

    it('YCC-593:[GET] - Get event by code - [Type: Condition] [vehicleType: Railcar]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventCondition.code}?eventType=${payloadRailcarEventCondition.type}&vehicleType=${payloadRailcarEventCondition.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadRailcarEventCondition);
            });
    });

    it('YCC-418:[GET] - Get event by code - [Type: Responsible] [vehicleType: Locomotive]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventResponsible.code}?eventType=${payloadLocomotiveEventResponsible.type}&vehicleType=${payloadLocomotiveEventResponsible.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadLocomotiveEventResponsible);
            });
    });

    it('YCC-581:[GET] - Get event by code - [Type: Availability] [vehicleType: Locomotive]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventAvailability.code}?eventType=${payloadLocomotiveEventAvailability.type}&vehicleType=${payloadLocomotiveEventAvailability.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadLocomotiveEventAvailability);
            });
    });

    it('YCC-582:[GET] - Get event by code - [Type: Location] [vehicleType: Locomotive]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventLocation.code}?eventType=${payloadLocomotiveEventLocation.type}&vehicleType=${payloadLocomotiveEventLocation.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadLocomotiveEventLocation);
            });
    });

    it('YCC-583:[GET] - Get event by code - [Type: State] [vehicleType: Locomotive]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventState.code}?eventType=${payloadLocomotiveEventState.type}&vehicleType=${payloadLocomotiveEventState.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadLocomotiveEventState);
            });
    });

    it('YCC-584:[GET] - Get event by code - [Type: Exchange] [vehicleType: Locomotive]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventExchange.code}?eventType=${payloadLocomotiveEventExchange.type}&vehicleType=${payloadLocomotiveEventExchange.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadLocomotiveEventExchange);
            });
    });

    it('YCC-585:[GET] - Get event by code - [Type: Cargo] [vehicleType: Locomotive]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventCargo.code}?eventType=${payloadLocomotiveEventCargo.type}&vehicleType=${payloadLocomotiveEventCargo.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadLocomotiveEventCargo);
            });
    });

    it('YCC-586:[GET] - Get event by code - [Type: Condition] [vehicleType: Locomotive]', () => {
        Requests.getPayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventCondition.code}?eventType=${payloadLocomotiveEventCondition.type}&vehicleType=${payloadLocomotiveEventCondition.vehicleType}`)
            .should((responseGetEventByCode) => {
                expect(responseGetEventByCode.status).to.eq(200);
                expect(responseGetEventByCode.body).to.deep.equal(payloadLocomotiveEventCondition);
            });
    });
    
    after(() => {
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventResponsible.code}?eventType=${payloadRailcarEventResponsible.type}&vehicleType=${payloadRailcarEventResponsible.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventAvailability.code}?eventType=${payloadRailcarEventAvailability.type}&vehicleType=${payloadRailcarEventAvailability.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventLocation.code}?eventType=${payloadRailcarEventLocation.type}&vehicleType=${payloadRailcarEventLocation.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventState.code}?eventType=${payloadRailcarEventState.type}&vehicleType=${payloadRailcarEventState.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventExchange.code}?eventType=${payloadRailcarEventExchange.type}&vehicleType=${payloadRailcarEventExchange.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventCargo.code}?eventType=${payloadRailcarEventCargo.type}&vehicleType=${payloadRailcarEventCargo.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventCondition.code}?eventType=${payloadRailcarEventCondition.type}&vehicleType=${payloadRailcarEventCondition.vehicleType}`);

        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventResponsible.code}?eventType=${payloadLocomotiveEventResponsible.type}&vehicleType=${payloadLocomotiveEventResponsible.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventAvailability.code}?eventType=${payloadLocomotiveEventAvailability.type}&vehicleType=${payloadLocomotiveEventAvailability.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventLocation.code}?eventType=${payloadLocomotiveEventLocation.type}&vehicleType=${payloadLocomotiveEventLocation.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventState.code}?eventType=${payloadLocomotiveEventState.type}&vehicleType=${payloadLocomotiveEventState.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventExchange.code}?eventType=${payloadLocomotiveEventExchange.type}&vehicleType=${payloadLocomotiveEventExchange.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventCargo.code}?eventType=${payloadLocomotiveEventCargo.type}&vehicleType=${payloadLocomotiveEventCargo.vehicleType}`);
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadLocomotiveEventCondition.code}?eventType=${payloadLocomotiveEventCondition.type}&vehicleType=${payloadLocomotiveEventCondition.vehicleType}`);
    });

});