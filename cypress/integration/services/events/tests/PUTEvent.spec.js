import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarEventResponsible = require('../payloads/railcar-event-type-responsible.json');
const eventBaseUrl = 'Events';

var token = '';

describe('PUT Event', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, eventBaseUrl, payloadRailcarEventResponsible);
        });
    });

    it('YCC-419 :: Version : 1 :: [PUT] - Updates a event by code - [Type: Responsible] [vehicleType: Railcar]', () => {
        var payloadRailcarEventResponsibleUpdated = payloadRailcarEventResponsible;
        payloadRailcarEventResponsibleUpdated.description = "Updated";

        Requests.updatePayloadByCode(token, `${eventBaseUrl}/${payloadRailcarEventResponsible.code}`, payloadRailcarEventResponsibleUpdated).should((responseUpdateEvent) => {
            expect(responseUpdateEvent.status).to.eq(200);
            Requests.getPayloadByCode(token, 
                `${eventBaseUrl}/${payloadRailcarEventResponsible.code}?eventType=${payloadRailcarEventResponsible.type}&vehicleType=${payloadRailcarEventResponsible.vehicleType}`)
                .should((responseGetEventByCode) => {
                    if(responseGetEventByCode.status == 200){
                        expect(responseGetEventByCode.body).to.deep.equal(payloadRailcarEventResponsibleUpdated);
                    }
                });
        });
    });
  
    after(() => {
        Requests.deletePayloadByCode(token, 
            `${eventBaseUrl}/${payloadRailcarEventResponsible.code}?eventType=${payloadRailcarEventResponsible.type}&vehicleType=${payloadRailcarEventResponsible.vehicleType}`);
    });

});