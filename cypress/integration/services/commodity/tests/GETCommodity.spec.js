import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadCommodity = require('../payloads/commodity.json');
const payloadCommodity2 = require('../payloads/commodity-2.json');
const commodityBaseUrl = 'Commodity';
var token = '';

describe('GET Commodity', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addMultiplePayloads(token, commodityBaseUrl+"/commodities", [payloadCommodity, payloadCommodity2]);
        });
    });

    it('YCC-404 - [GET] - Get all commodities', () => {
        Requests.getAllPayloads(token, commodityBaseUrl).should((responseGetAllCommodities) => {
            expect(responseGetAllCommodities.status).to.eq(200);
            expect(responseGetAllCommodities.body).to.be.not.null;

            var responsePayloadCommodity1 = '';
            var responsePayloadCommodity2 = '';
            responseGetAllCommodities.body.results.map(function(commodity, i){
                if(payloadCommodity.code == commodity.code){
                    responsePayloadCommodity1 = commodity;
                }
                if(payloadCommodity2.code == commodity.code){
                    responsePayloadCommodity2 = commodity;
                }
            });

            expect(responsePayloadCommodity1).to.deep.equal(payloadCommodity);
            expect(responsePayloadCommodity2).to.deep.equal(payloadCommodity2);
        });
    });

    it('YCC-406 - [GET] - Get commodity by code', () => {
        Requests.getPayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`).should((responseGetCommodity) => {
            expect(responseGetCommodity.status).to.eq(200);
            expect(responseGetCommodity.body).to.be.not.null;
            expect(responseGetCommodity.body).to.deep.equal(payloadCommodity);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`);
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity2.code}`);
    });

});