import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadCommodity = require('../payloads/commodity.json');
const payloadCommodity2 = require('../payloads/commodity-2.json');
const commodityBaseUrl = 'Commodity';
var token = '';

describe('POST Commodity', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-405 - [POST] - Creates a new commodity', () => {
        Requests.addPayload(token, commodityBaseUrl, payloadCommodity).should((responseAddCommodity) => {
            expect(responseAddCommodity.status).to.eq(200);
        });
    });

    it('YCC-409 - [POST] - Creates multiple commodities', () => {
        Requests.addMultiplePayloads(token, commodityBaseUrl+"/commodities", [payloadCommodity, payloadCommodity2]).should((responseAddMultipleCommodities) => {
            expect(responseAddMultipleCommodities.status).to.eq(200);
        });
    });

    afterEach(() => {
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`);
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity2.code}`);
    });
    
});