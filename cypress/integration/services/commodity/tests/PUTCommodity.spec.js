import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadCommodity = require('../payloads/commodity.json');
const commodityBaseUrl = 'Commodity';
var token = '';

describe('PUT Commodity', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, commodityBaseUrl, payloadCommodity);
        });
    });

    it('YCC-407 - [PUT] - Updates a commodity by code', () => {
        var payloadCommodityUpdated = payloadCommodity;
        payloadCommodityUpdated.description = "CYPRESS_COMMODITY DESCRIPTION UPDATED";
        payloadCommodityUpdated.label = "UP_LAB";

        Requests.updatePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`, payloadCommodityUpdated).should((responseUpdateCommodity) => {
            expect(responseUpdateCommodity.status).to.eq(200);
            Requests.getPayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`).should((responseGetCommodity) => {
                if(responseGetCommodity.status == 200){
                    expect(responseGetCommodity.body).to.deep.equal(payloadCommodity);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`);
    });

});