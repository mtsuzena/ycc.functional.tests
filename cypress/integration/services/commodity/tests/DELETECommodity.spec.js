import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadCommodity = require('../payloads/commodity.json');
const commodityBaseUrl = 'Commodity';
var token = '';

describe('DELETE Commodity', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, commodityBaseUrl, payloadCommodity);
        });
    });

    it('YCC-408 - [DELETE] - Delete an existent commodity by code', () => {
        Requests.deletePayloadByCode(token, `${commodityBaseUrl}/${payloadCommodity.code}`).should((responseDeleteCommodity) => {
            expect(responseDeleteCommodity.status).to.eq(200);
        });
    });

});