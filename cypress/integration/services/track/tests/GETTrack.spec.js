import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../payloads/track-mnb.json');
const payloadTrack2 = require('../payloads/track.json');
const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
var token = '';

describe('GET Track', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, `${YardBaseUrl}`, payloadYard);
            Requests.addPayload(token, `${GaugeBaseUrl}`, payloadGauge);
            Requests.addMultiplePayloads(token, TrackBaseUrl+"/tracks", [payloadTrack, payloadTrack2])
        });
    });

    it('YCC-486 - [GET] - Get all tracks', () => {
        Requests.getAllPayloads(token, TrackBaseUrl).should((responseGetAllTracks) => {
            expect(responseGetAllTracks.status).to.eq(200);
            expect(responseGetAllTracks.body).to.be.not.null;

            var responsePayloadTrack = '';
            var responsePayloadTrack2 = '';

            responseGetAllTracks.body.results.map(function(track, i){
                if(payloadTrack.code == track.code){
                    responsePayloadTrack = track;
                }
                if(payloadTrack2.code == track.code){
                    responsePayloadTrack2 = track;
                }
            });

            expect(responsePayloadTrack).to.deep.equal(payloadTrack);
            expect(responsePayloadTrack2).to.deep.equal(payloadTrack2);
        });
    });

    it('YCC-488 - [GET] - Get track by code', () => {
        Requests.getPayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`).should((responseGetTrackByCode) => {
            expect(responseGetTrackByCode.status).to.eq(200);
            expect(responseGetTrackByCode.body).to.be.not.null;
            expect(responseGetTrackByCode.body).to.deep.equal(payloadTrack);
        });
    });
 
    after(() => {
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack2.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });

});