import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../payloads/track-mnb.json');
const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
var token = '';

describe('GET Track', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
        });
    });

    it('YCC-489 - [PUT] - Updates a track by code', () => {
        var payloadTrackUpdated = payloadTrack;
        payloadTrackUpdated.name = "Standard Switching Track-UPD";
        payloadTrackUpdated.length = 2000.0;
        payloadTrackUpdated.isWorkshop = true;
        payloadTrackUpdated.isIndustry = true;
        payloadTrackUpdated.isFlipped = true;
        payloadTrackUpdated.isBlocked = true;

        Requests.updatePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`, payloadTrackUpdated).should((responseUpdateTrack) => {
            expect(responseUpdateTrack.status).to.eq(200);
            Requests.getPayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`).should((responseGetTrackByCode) => {
                if(responseGetTrackByCode.status == 200){
                    expect(responseGetTrackByCode.body).to.deep.equal(payloadTrackUpdated);
                }
            });
        });
    });
 
    after(() => {
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });

});