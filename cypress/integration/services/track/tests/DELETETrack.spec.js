import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../payloads/track-mnb.json');
const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
var token = '';

describe('DELETE Track', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, `${YardBaseUrl}`, payloadYard);
            Requests.addPayload(token, `${GaugeBaseUrl}`, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
        });
    });

    it('YCC-490 - [DELETE] - Delete an existent track by code', () => {
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`).should((responseDeleteTrack) => {
            expect(responseDeleteTrack.status).to.eq(200);
        });
    });

    it('YCC-599 :: Version : 1 :: [DELETE] - Delete an existent track by code - inexistent code', () => {
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/INEXISTENT_CODE`).should((responseDeleteTrack) => {
            expect(responseDeleteTrack.status).to.eq(404);
        });
    });
 
    after(() => {
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });

});