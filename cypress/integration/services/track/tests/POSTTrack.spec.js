import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../payloads/track-mnb.json');
const payloadTrack2 = require('../payloads/track.json');
var token = '';
const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';

describe('POST Track', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, `${YardBaseUrl}`, payloadYard);
            Requests.addPayload(token, `${GaugeBaseUrl}`, payloadGauge);
        });
    });

    it('YCC-487 - [POST] - Creates a new track', () => {
        Requests.addPayload(token, TrackBaseUrl, payloadTrack).should((responseAddTrack) => {
            expect(responseAddTrack.status).to.eq(200);
        });
    });

    it('YCC-491 - [POST] - Creates multiple tracks', () => {
        Requests.addMultiplePayloads(token, TrackBaseUrl+"/tracks", [payloadTrack, payloadTrack2]).should((responseAddMultipleTracks) => {
            expect(responseAddMultipleTracks.status).to.eq(200);
        });
    });

    afterEach(() => {
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack2.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });

});