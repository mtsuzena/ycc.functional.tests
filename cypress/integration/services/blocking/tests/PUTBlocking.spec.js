import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadBlockingYard = require('../payloads/blocking-yard.json');
const payloadBlockingTrain = require('../payloads/blocking-train.json');

const blockingBaseUrl = 'Blocking';

var token = '';

describe('PUT Blocking', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addMultiplePayloads(token, `${blockingBaseUrl}/blockings`, [payloadBlockingYard, payloadBlockingTrain]);
        });
    });

    it('YCC-596 :: Version : 1 :: [PUT] - Updates a blocking - Train', () => {
        var payloadBlockingTrainUpdated = payloadBlockingTrain;
        payloadBlockingTrainUpdated.description = "Updated";

        Requests.updatePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingTrain.code}`, payloadBlockingTrainUpdated).should((responseUpdateBlockingTrain) => {
            expect(responseUpdateBlockingTrain.status).to.eq(200);
            Requests.getPayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingTrain.code}?blockType=${payloadBlockingTrain.type}`).should((responseGetBlockingTrain) => {
                if(responseGetBlockingTrain.status == 200){
                    expect(responseGetBlockingTrain.body).to.deep.equal(payloadBlockingTrainUpdated);
                }
            });
        });
    });

    it('YCC-400 :: Version : 1 :: [PUT] - Updates a blocking - Yard', () => {
        var payloadBlockingYardUpdated = payloadBlockingYard;
        payloadBlockingYardUpdated.description = "Updated";

        Requests.updatePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingYard.code}`, payloadBlockingYardUpdated).should((responseUpdateBlockingYard) => {
            expect(responseUpdateBlockingYard.status).to.eq(200);
            Requests.getPayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingYard.code}?blockType=${payloadBlockingYard.type}`).should((responseGetBlockingYard) => {
                if(responseGetBlockingYard.status == 200){
                    expect(responseGetBlockingYard.body).to.deep.equal(payloadBlockingYardUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingYard.code}?blockType=${payloadBlockingYard.type}`);
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingTrain.code}?blockType=${payloadBlockingTrain.type}`);
    });

});