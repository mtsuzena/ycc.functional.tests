import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadBlockingYard = require('../payloads/blocking-yard.json');
const payloadBlockingTrain = require('../payloads/blocking-train.json');

const blockingBaseUrl = 'Blocking';

var token = '';

describe('GET Blocking', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addMultiplePayloads(token, `${blockingBaseUrl}/blockings`, [payloadBlockingYard, payloadBlockingTrain]);
        });
    });

    it('YCC-397 :: Version : 1 :: [GET] - Get all blockings', () => {
        Requests.getAllPayloads(token, blockingBaseUrl).should((responseGetAllBlockings) => {
            expect(responseGetAllBlockings.status).to.eq(200);

            var responsePayloadBlockingYard = '';
            var responsePayloadBlockingTrain = '';

            responseGetAllBlockings.body.results.map((blocking) => {
                if(payloadBlockingYard.code == blocking.code){
                    responsePayloadBlockingYard = blocking;
                }
                if(payloadBlockingTrain.code == blocking.code){
                    responsePayloadBlockingTrain = blocking;
                }
            });

            expect(responsePayloadBlockingYard).to.deep.equal(payloadBlockingYard);
            expect(responsePayloadBlockingTrain).to.deep.equal(payloadBlockingTrain);
        });
    });

    it('YCC-595 :: Version : 1 :: [GET] - Get the blocking by number - Train', () => {
        Requests.getPayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingTrain.code}?blockType=${payloadBlockingTrain.type}`).should((responseGetBlockingTrain) => {
            expect(responseGetBlockingTrain.status).to.eq(200);
        });
    });

    it('YCC-399 :: Version : 1 :: [GET] - Get the blocking by number - Yard', () => {
        Requests.getPayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingYard.code}?blockType=${payloadBlockingYard.type}`).should((responseGetBlockingYard) => {
            expect(responseGetBlockingYard.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingYard.code}?blockType=${payloadBlockingYard.type}`);
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingTrain.code}?blockType=${payloadBlockingTrain.type}`);
    });

});