import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadBlockingYard = require('../payloads/blocking-yard.json');
const payloadBlockingTrain = require('../payloads/blocking-train.json');

const blockingBaseUrl = 'Blocking';

var token = '';

describe('DELETE Blocking', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, blockingBaseUrl, payloadBlockingYard);
            Requests.addPayload(token, blockingBaseUrl, payloadBlockingTrain);
        });
    });

    it('YCC-401 :: Version : 1 :: [DELETE] - Delete a blocking by code - Yard', () => {
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingYard.code}?blockType=${payloadBlockingYard.type}`).should((responseDeleteBlockingYard) => {
            expect(responseDeleteBlockingYard.status).to.eq(200);
        });
    });

    it('YCC-597 :: Version : 1 :: [DELETE] - Delete a blocking by code - Train', () => {
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingTrain.code}?blockType=${payloadBlockingTrain.type}`).should((responseDeleteBlockingTrain) => {
            expect(responseDeleteBlockingTrain.status).to.eq(200);
        });
    });

});