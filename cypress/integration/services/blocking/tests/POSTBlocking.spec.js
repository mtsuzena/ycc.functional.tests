import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadBlockingYard = require('../payloads/blocking-yard.json');
const payloadBlockingTrain = require('../payloads/blocking-train.json');
const payloadBlockingYard2 = require('../payloads/blocking-yard-2.json');
const payloadBlockingTrain2 = require('../payloads/blocking-train-2.json');

const blockingBaseUrl = 'Blocking';

var token = '';

describe('POST Blocking', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-398 :: Version : 1 :: [POST] - Creates a new blocking - Yard', () => {
        Requests.addPayload(token, blockingBaseUrl, payloadBlockingYard).should((responseAddBlockingYard) => {
            expect(responseAddBlockingYard.status).to.eq(200);
        });
    });

    it('YCC-594 :: Version : 1 :: [POST] - Creates a new blocking - Train', () => {
        Requests.addPayload(token, blockingBaseUrl, payloadBlockingTrain).should((responseAddBlockingTrain) => {
            expect(responseAddBlockingTrain.status).to.eq(200);
        });
    });

    it('YCC-402 :: Version : 1 :: [POST] - Creates multiple blockings', () => {
        Requests.addMultiplePayloads(token, `${blockingBaseUrl}/blockings`, [payloadBlockingYard2, payloadBlockingTrain2]).should((responseAddMultipleBlockings) => {
            expect(responseAddMultipleBlockings.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingYard.code}?blockType=${payloadBlockingYard.type}`);
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingTrain.code}?blockType=${payloadBlockingTrain.type}`);
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingYard2.code}?blockType=${payloadBlockingYard2.type}`);
        Requests.deletePayloadByCode(token, `${blockingBaseUrl}/${payloadBlockingTrain2.code}?blockType=${payloadBlockingTrain2.type}`);
    });

});