import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadIndustry = require('../payloads/industry.json');
const industrytBaseUrl = 'Industry';
var token = '';

describe('PUT Industry', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, industrytBaseUrl, payloadIndustry);
        });
    });

    it('YCC-436 - [PUT] - Updates a industry by code', () => {
        var payloadIndustryUpdated = payloadIndustry;
        payloadIndustryUpdated.name = "Industry Cypress Name Updated";
        payloadIndustryUpdated.description = "Industry Cypress desc Updated";
        payloadIndustryUpdated.label = "up_lab";

        Requests.updatePayloadByCode(token, `${industrytBaseUrl}/${payloadIndustry.code}`, payloadIndustryUpdated).should((responseUpdatedIndustry) => {
            expect(responseUpdatedIndustry.status).to.eq(200);
            Requests.getPayloadByCode(token, `${industrytBaseUrl}/${payloadIndustry.code}`).should((responseGetIndustry) => {
                if(responseGetIndustry.status == 200){
                    expect(responseGetIndustry.body).to.deep.equal(payloadIndustryUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${industrytBaseUrl}/${payloadIndustry.code}`);
    });

});