import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadIndustry = require('../payloads/industry.json');
const industrytBaseUrl = 'Industry';
var token = '';

describe('POST Industry', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-434 - [POST] - Creates a new industry', () => {
        Requests.addPayload(token, industrytBaseUrl, payloadIndustry).should((responseAddIndustry) => {
            expect(responseAddIndustry.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${industrytBaseUrl}/${payloadIndustry.code}`);
    });

});