import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadIndustry = require('../payloads/industry.json');
const payloadIndustry2 = require('../payloads/industry-2.json');
const industrytBaseUrl = 'Industry';
var token = '';

describe('GET Industry', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, industrytBaseUrl, payloadIndustry);
            Requests.addPayload(token, industrytBaseUrl, payloadIndustry2);
        });
    });

    it('YCC-433 - [GET] - Get all industries', () => {
        Requests.getAllPayloads(token, industrytBaseUrl).should((responseGetAllIndustries) => {
            expect(responseGetAllIndustries.status).to.eq(200);
            expect(responseGetAllIndustries.body).to.be.not.null;

            var responsePayloadIndustry1 = '';
            var responsePayloadIndustry2 = '';

            responseGetAllIndustries.body.results.map(function(industry, i){
                if(payloadIndustry.code == industry.code){
                    responsePayloadIndustry1 = industry;
                }
                if(payloadIndustry2.code == industry.code){
                    responsePayloadIndustry2 = industry;
                }
            });

            expect(responsePayloadIndustry1).to.deep.equal(payloadIndustry);
            expect(responsePayloadIndustry2).to.deep.equal(payloadIndustry2);
        });
    });

    it('YCC-435 - [GET] - Get industry by code', () => {
        Requests.getPayloadByCode(token, `${industrytBaseUrl}/${payloadIndustry.code}`).should((responseGetIndustry) => {
            expect(responseGetIndustry.status).to.eq(200);
            expect(responseGetIndustry.body).to.be.not.null;
            expect(responseGetIndustry.body).to.deep.equal(payloadIndustry);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${industrytBaseUrl}/${payloadIndustry.code}`);
        Requests.deletePayloadByCode(token, `${industrytBaseUrl}/${payloadIndustry2.code}`);
    });

});