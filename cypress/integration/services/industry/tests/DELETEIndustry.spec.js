import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadIndustry = require('../payloads/industry.json');
const industrytBaseUrl = 'Industry';
var token = '';

describe('DELETE Industry', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, industrytBaseUrl, payloadIndustry);
        });
    });

    it('YCC-437 - [DELETE] - Delete an existent industry by code', () => {
        Requests.deletePayloadByCode(token, `${industrytBaseUrl}/${payloadIndustry.code}`).should((responseDeleteIndustry) => {
            expect(responseDeleteIndustry.status).to.eq(200);
        });
    });

});