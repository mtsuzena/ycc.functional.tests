import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotiveModel = require('../payloads/locomotive-model.json');
const payloadLocomotiveType = require('../../locomotiveType/payloads/locomotive-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');

const LocomotiveModelBaseUrl = 'LocomotiveModel';
const LocomotiveTypeBaseUrl = 'LocomotiveType';
const GaugeBaseUrl = 'Gauge';

var token = '';

describe('POST Locomotive Model', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
        });
    });

    it('YCC-451 - [POST] - Creates a new locomotive model', () => {
        Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel).should((responseAddLocomotiveModel) => {
            expect(responseAddLocomotiveModel.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});