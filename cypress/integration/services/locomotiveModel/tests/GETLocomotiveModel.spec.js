import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotiveModel = require('../payloads/locomotive-model.json');
const payloadLocomotiveModel2 = require('../payloads/locomotive-model-2.json');
const payloadLocomotiveType = require('../../locomotiveType/payloads/locomotive-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');

const LocomotiveModelBaseUrl = 'LocomotiveModel';
const LocomotiveTypeBaseUrl = 'LocomotiveType';
const GaugeBaseUrl = 'Gauge';

var token = '';

describe('GET Locomotive Model', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel);
            Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel2);
        });
    });

    it('YCC-450 - [GET] - Get all locomotive models', () => {
        Requests.getAllPayloads(token, LocomotiveModelBaseUrl).should((responseGetAllLocomotiveModels) => {
            expect(responseGetAllLocomotiveModels.status).to.eq(200);

            var responsePayloadLocomotiveModel = '';
            var responsePayloadLocomotiveModel2 = '';

            responseGetAllLocomotiveModels.body.results.map((locomotiveModel) => {
                if(payloadLocomotiveModel.code == locomotiveModel.code){
                    responsePayloadLocomotiveModel = locomotiveModel;
                }
                if(payloadLocomotiveModel2.code == locomotiveModel.code){
                    responsePayloadLocomotiveModel2 = locomotiveModel;
                }
            });

            expect(responsePayloadLocomotiveModel).to.deep.equal(payloadLocomotiveModel);
            expect(responsePayloadLocomotiveModel2).to.deep.equal(payloadLocomotiveModel2);
        });
    });

    it('YCC-452 - [GET] - Get locomotive model by code', () => {
        Requests.getPayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`).should((responseGetLocomotiveModel) => {
            expect(responseGetLocomotiveModel.status).to.eq(200);
            expect(responseGetLocomotiveModel.body).to.deep.equal(payloadLocomotiveModel);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel2.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});