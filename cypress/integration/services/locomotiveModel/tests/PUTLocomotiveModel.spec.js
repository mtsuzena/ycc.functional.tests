import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotiveModel = require('../payloads/locomotive-model.json');
const payloadLocomotiveType = require('../../locomotiveType/payloads/locomotive-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');

const LocomotiveModelBaseUrl = 'LocomotiveModel';
const LocomotiveTypeBaseUrl = 'LocomotiveType';
const GaugeBaseUrl = 'Gauge';

var token = '';

describe('PUT Locomotive Model', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel);
        });
    });

    it('YCC-453 - [PUT] - Updates a locomotive model by code', () => {
        var payloadLocomotiveModelUpdated = payloadLocomotiveModel;
        payloadLocomotiveModelUpdated.length = 1.5;
        payloadLocomotiveModelUpdated.tare = 2.5;
        payloadLocomotiveModelUpdated.axles = 2;

        Requests.updatePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`, payloadLocomotiveModelUpdated).should((responseUpdatedLocomotiveModel) => {
            expect(responseUpdatedLocomotiveModel.status).to.eq(200);
            Requests.getPayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`).should((responseGetLocomotiveModel) => {
                if(responseGetLocomotiveModel.status == 200){
                    expect(responseGetLocomotiveModel.body).to.deep.equal(payloadLocomotiveModelUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});