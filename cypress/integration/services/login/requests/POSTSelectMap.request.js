/// <reference types="cypress" />

function authenticateWithMap(mapInfo, token){
    return cy.request({
        method: 'POST',
        url: `Login/${mapInfo}`,
        failOnStatusCode: false,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}

export { authenticateWithMap };