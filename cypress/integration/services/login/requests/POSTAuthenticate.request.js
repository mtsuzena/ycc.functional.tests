/// <reference types="cypress" />

const payloadAuthenticate = require('../payloads/authenticate.json');

function authenticate(){
    return cy.request({
        method: 'POST',
        url: 'Login/Authenticate',
        failOnStatusCode: false,
        body: payloadAuthenticate
    })
}

export { authenticate };