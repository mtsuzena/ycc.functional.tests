import * as POSTAuthenticate from '../requests/POSTAuthenticate.request';
import * as JWTHelper from '../../../helpers/jwt.helper';

describe('POST Authenticate', () => {

    it('YCC-460:[POST] - Authenticate', () => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            expect(responseAuthenticate.status).to.eq(200);
            expect(responseAuthenticate.body).to.be.not.null;
            const token = responseAuthenticate.body.accessToken
            cy.log("The Token is: " + token)
            const verified = JWTHelper.getPayload(token);
            expect(verified.unique_name[0]).to.eq("ycc")
        });
    });

});
