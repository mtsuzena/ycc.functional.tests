import * as POSTSelectMap from '../requests/POSTSelectMap.request';
import * as POSTAuthenticate from '../requests/POSTAuthenticate.request';
import * as JWTHelper from '../../../helpers/jwt.helper';
import * as Requests from '../../../helpers/requests.request';

const MapBaseUrl = 'Map';
const payloadMap = require('../../map/payloads/map.json');

var token = '';

describe('POST SelectMap', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, MapBaseUrl, payloadMap);
        });
    });

    it('YCC-461:[POST] - Select Map - PerformSwitches', () => {
        POSTSelectMap.authenticateWithMap(`SelectMap?mapCode=${payloadMap.code}&permission=PerformSwitches`, token).should((responseAuthenticateWithMap) => {
            expect(responseAuthenticateWithMap.status).to.eq(200);
            expect(responseAuthenticateWithMap.body).to.be.not.null;
            const tokenWithMap = JWTHelper.getPayload(responseAuthenticateWithMap.body.accessToken);
            expect(tokenWithMap.groupsid).to.eq(payloadMap.code);
            expect(tokenWithMap.role).to.eq("PerformSwitches");
            cy.log("The Token is: " + responseAuthenticateWithMap.body.accessToken);
        });
    });

    it('YCC-551:[POST] - Select Map - ViewOnly', () => {
        POSTSelectMap.authenticateWithMap(`SelectMap?mapCode=${payloadMap.code}&permission=ViewOnly`, token).should((responseAuthenticateWithMap) => {
            expect(responseAuthenticateWithMap.status).to.eq(200);
            expect(responseAuthenticateWithMap.body).to.be.not.null;
            const tokenWithMap = JWTHelper.getPayload(responseAuthenticateWithMap.body.accessToken);
            expect(tokenWithMap.groupsid).to.eq(payloadMap.code);
            expect(tokenWithMap.role).to.eq("ViewOnly")
            cy.log("The Token is: " + responseAuthenticateWithMap.body.accessToken);
        });
    });

    it('YCC-565 :: Version : 1 :: [POST] - Select Map - mapCode inexistent ', () => {
        POSTSelectMap.authenticateWithMap(`SelectMap?mapCode=MAP_COD_INEXISTENT&permission=ViewOnly`, token).should((responseAuthenticateWithMap) => {
            expect(responseAuthenticateWithMap.status).to.eq(404);
            expect(responseAuthenticateWithMap.body.errorCode).to.eq("MapNotFound");
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
    });

});


