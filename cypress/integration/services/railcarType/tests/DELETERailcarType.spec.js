import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarType = require('../payloads/railcar-type.json');
var token = '';
const RailcarTypeBaseUrl = 'RailCarType'

describe('DELETE Railcar Type', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
        });
    });

    it('YCC-485 - [DELETE] - Delete an existent railcar type by code', () => {
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`).should((responseDeleteRailcarType) => {
            expect(responseDeleteRailcarType.status).to.eq(200);
        });
    });

});