import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarType = require('../payloads/railcar-type.json');
var token = '';
const RailcarTypeBaseUrl = 'RailCarType'

describe('PUT Railcar Type', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
        });
    });

    it('YCC-484 - [PUT] - Updates a railcar type by code', () => {
        var payloadRailcarTypeUpdated = payloadRailcarType;
        Requests.updatePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`, payloadRailcarTypeUpdated).should((responseUpdateRailcarType) => {
            expect(responseUpdateRailcarType.status).to.eq(200);
            Requests.getPayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`).should((responseGetRailcarByCode) => {
                if(responseGetRailcarByCode.status == 200){
                    expect(responseGetRailcarByCode.body).to.deep.equal(payloadRailcarTypeUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});