import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarType = require('../payloads/railcar-type.json');
const payloadRailcarType2 = require('../payloads/railcar-type-2.json');
var token = '';
const RailcarTypeBaseUrl = 'RailCarType'

describe('GET Railcar Type', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType2);
        });
    });

    it('YCC-481 - [GET] - Get all railcar types', () => {
        Requests.getAllPayloads(token, RailcarTypeBaseUrl).should((responseGetAllRailcarTypes) => {
            expect(responseGetAllRailcarTypes.status).to.eq(200);
            expect(responseGetAllRailcarTypes.body).to.be.not.null;

            var responsePayloadRailcarType = '';
            var responsePayloadRailcarType2 = '';

            responseGetAllRailcarTypes.body.results.map(function(railcarType, i){
                if(payloadRailcarType.code == railcarType.code){
                    responsePayloadRailcarType = railcarType;
                }
                if(payloadRailcarType2.code == railcarType.code){
                    responsePayloadRailcarType2 = railcarType;
                }
            });

            expect(responsePayloadRailcarType).to.deep.equal(payloadRailcarType);
            expect(responsePayloadRailcarType2).to.deep.equal(payloadRailcarType2);
        });
    });

    it('YCC-483 - [GET] - Get railcar type by code', () => {
        Requests.getPayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`).should((responseGetRailcarByCode) => {
            expect(responseGetRailcarByCode.status).to.eq(200);
            expect(responseGetRailcarByCode.body).to.be.not.null;
            expect(responseGetRailcarByCode.body).to.deep.equal(payloadRailcarType);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType2.code}`);
    });

});