import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarType = require('../payloads/railcar-type.json');
var token = '';
const RailcarTypeBaseUrl = 'RailCarType'

describe('POST Railcar Type', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-482 - [POST] - Creates a new railcar type', () => {
        Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType).should((responseAddRailcarType) => {
            expect(responseAddRailcarType.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});