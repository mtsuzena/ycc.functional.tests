import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';
import * as POSTSelectMap from '../../login/requests/POSTSelectMap.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../../map/payloads/map.json');
const payloadActivity = require('../payloads/activity.json');
const payloadTaskMessage = require('../payloads/task-message.json');

const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
const MapBaseUrl = 'Map';
const activityBaseUrl = 'Activity';

var token = '';
var tokenWithMap = '';
let activityId = 1;

describe('POST Activity', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
            Requests.addPayload(token, MapBaseUrl, payloadMap);
            POSTSelectMap.authenticateWithMap(`SelectMap?mapCode=${payloadMap.code}&permission=PerformSwitches`, token).should((responseAuthenticateWithMap) => {
                tokenWithMap = responseAuthenticateWithMap.body.accessToken;
            });
        });
    });

    it('YCC-374 :: Version : 1 :: [POST] - Creates a new activity', () => {
        Requests.addPayload(tokenWithMap, activityBaseUrl, payloadActivity).should((responseAddActivity) => {
            expect(responseAddActivity.status).to.eq(200);
            activityId = responseAddActivity.body.id;
            expect(payloadActivity.label).to.eq(responseAddActivity.body.label);
        });
    });

    it('YCC-382 :: Version : 1 :: [POST] - Creates a new Task/Message on activity', () => {
        Requests.addPayload(tokenWithMap, `${activityBaseUrl}/${activityId}/taskMessages`, payloadTaskMessage).should((responseAddTaskMessage) => {
            expect(responseAddTaskMessage.status).to.eq(200);
            expect(responseAddTaskMessage.body.description).to.eq(payloadTaskMessage.description);
            expect(responseAddTaskMessage.body.switchOrderType).to.eq(payloadTaskMessage.switchOrderType);
            expect(responseAddTaskMessage.body.estimatedStart).to.eq(payloadTaskMessage.estimatedStart);
            expect(responseAddTaskMessage.body.estimatedEnd).to.eq(payloadTaskMessage.estimatedEnd);
            expect(responseAddTaskMessage.body.notes).to.eq(payloadTaskMessage.notes);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(tokenWithMap, `${activityBaseUrl}/${activityId}`);
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });
});