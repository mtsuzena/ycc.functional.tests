import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';
import * as POSTSelectMap from '../../login/requests/POSTSelectMap.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../../map/payloads/map.json');
const payloadActivity = require('../payloads/activity.json');
const payloadTaskMessage = require('../payloads/task-message.json');

const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
const MapBaseUrl = 'Map';
const activityBaseUrl = 'Activity';

let token = '';
let tokenWithMap = '';
let activityId = 1;
let taskMessageId = 1;

describe('PUT Activity', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
            Requests.addPayload(token, MapBaseUrl, payloadMap);
            POSTSelectMap.authenticateWithMap(`SelectMap?mapCode=${payloadMap.code}&permission=PerformSwitches`, token).should((responseAuthenticateWithMap) => {
                tokenWithMap = responseAuthenticateWithMap.body.accessToken;
                Requests.addPayload(tokenWithMap, activityBaseUrl, payloadActivity).should((responseAddActivity) => {
                    activityId = responseAddActivity.body.id;
                    Requests.addPayload(tokenWithMap, `${activityBaseUrl}/${activityId}/taskMessages`, payloadTaskMessage).should((responseAddTaskMessage) => {
                        taskMessageId = responseAddTaskMessage.body.id;
                    });
                });
            });
        });
    });

    it('YCC-375 :: Version : 1 :: [PUT] - Updates an activity by activityId', () => {
        let payloadActivityUpdated = payloadActivity;
        payloadActivityUpdated.label = "Updt";
        payloadActivityUpdated.notes = "NotesUpdtead";

        Requests.updatePayloadByCode(tokenWithMap, `${activityBaseUrl}/${activityId}`, payloadActivityUpdated).should((responseUpdateActivity) => {
            expect(responseUpdateActivity.status).to.eq(200);
            Requests.getPayloadByCode(tokenWithMap, `${activityBaseUrl}/${activityId}`).should((responseGetActivity) => {
                if(responseGetActivity.status == 200){
                    expect(responseGetActivity.body.label).to.eq(payloadActivityUpdated.label);
                    expect(responseGetActivity.body.notes).to.eq(payloadActivityUpdated.notes);
                }
            });
        });
    });

    it('YCC-383 :: Version : 1 :: [PUT] - Updates a new Task/Message on activity', () => {
        let payloadTaskMessageUpdated = payloadTaskMessage;
        payloadTaskMessageUpdated.notes = "NotesUpdtead";
        payloadTaskMessageUpdated.description = "DescUpdtead";

        Requests.updatePayloadByCode(tokenWithMap, `${activityBaseUrl}/${activityId}/taskMessages/${taskMessageId}`, payloadTaskMessageUpdated).should((responseUpdateTaskMessage) => {
            expect(responseUpdateTaskMessage.status).to.eq(200);
            expect(responseUpdateTaskMessage.body.id).to.eq(taskMessageId);
            expect(responseUpdateTaskMessage.body.notes).to.eq(payloadTaskMessageUpdated.notes);
            expect(responseUpdateTaskMessage.body.description).to.eq(payloadTaskMessageUpdated.description);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(tokenWithMap, `${activityBaseUrl}/${activityId}`);
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });
});