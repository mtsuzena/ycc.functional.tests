import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';
import * as POSTSelectMap from '../../login/requests/POSTSelectMap.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadTrack = require('../../track/payloads/track-mnb.json');
const payloadMap = require('../../map/payloads/map.json');
const payloadActivity = require('../payloads/activity.json');

const GaugeBaseUrl = 'Gauge';
const YardBaseUrl = 'Yard';
const TrackBaseUrl = 'Track';
const MapBaseUrl = 'Map';
const activityBaseUrl = 'Activity';

var token = '';
var tokenWithMap = '';
let activityId = 1;

describe('DELETE Activity', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, TrackBaseUrl, payloadTrack);
            Requests.addPayload(token, MapBaseUrl, payloadMap);
            POSTSelectMap.authenticateWithMap(`SelectMap?mapCode=${payloadMap.code}&permission=PerformSwitches`, token).should((responseAuthenticateWithMap) => {
                tokenWithMap = responseAuthenticateWithMap.body.accessToken;
                Requests.addPayload(tokenWithMap, activityBaseUrl, payloadActivity).should((responseAddActivity) => {
                    activityId = responseAddActivity.body.id;
                });
            });
        });
    });

    it('YCC-376 :: Version : 1 :: [DELETE] - Deletes an activity by activityId', () => {
        Requests.deletePayloadByCode(tokenWithMap, `${activityBaseUrl}/${activityId}`).should((responseDeleteActivity) => {
            expect(responseDeleteActivity.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${TrackBaseUrl}/${payloadTrack.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`);
    });
});