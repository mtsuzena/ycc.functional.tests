import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarModel = require('../payloads/railcar-model.json');
const payloadRailcarModel2 = require('../payloads/railcar-model-2.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
var token = '';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';

describe('POST Railcar Model', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
        });
    });

    it('YCC-476 - [POST] - Creates a new railcar model', () => {
        Requests.addPayload(token, RailcarModelBaseUrl, payloadRailcarModel).should((responseAddRailcarModel) => {
            expect(responseAddRailcarModel.status).to.eq(200);
        });
    });

    it('YCC-480 - [POST] - Creates multiple railcar models', () => {
        Requests.addMultiplePayloads(token, RailcarModelBaseUrl+"/models", [payloadRailcarModel, payloadRailcarModel2]).should((responseAddMultipleRailcarModels) => {
            expect(responseAddMultipleRailcarModels.status).to.eq(200);
        });
    });

    afterEach(() => {
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel2.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});