import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarModel = require('../payloads/railcar-model.json');
const payloadRailcarModel2 = require('../payloads/railcar-model-2.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
var token = '';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';

describe('GET Railcar Model', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addMultiplePayloads(token, RailcarModelBaseUrl+"/models", [payloadRailcarModel, payloadRailcarModel2]);
        });
    });

    it('YCC-475 - [GET] - Get all railcar models', () => {
        Requests.getAllPayloads(token, RailcarModelBaseUrl).should((responseGetAllRailcarModels) => {
            expect(responseGetAllRailcarModels.status).to.eq(200);
            expect(responseGetAllRailcarModels.body).to.be.not.null;

            var responsePayloadRailcarModel = '';
            var responsePayloadRailcarModel2 = '';

            responseGetAllRailcarModels.body.results.map((railcarModel, i) => {
                if(payloadRailcarModel.code == railcarModel.code){
                    responsePayloadRailcarModel = railcarModel;
                }
                if(payloadRailcarModel2.code == railcarModel.code){
                    responsePayloadRailcarModel2 = railcarModel;
                }
            });

            expect(responsePayloadRailcarModel).to.deep.equal(payloadRailcarModel);
            expect(responsePayloadRailcarModel2).to.deep.equal(payloadRailcarModel2);
        });
    });

    it('YCC-477 - [GET] - Get railcar model by code', () => {
        Requests.getPayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`).should((responseGetRailcarModelByCode) => {
            expect(responseGetRailcarModelByCode.status).to.eq(200);
            expect(responseGetRailcarModelByCode.body).to.be.not.null;
            expect(responseGetRailcarModelByCode.body).to.deep.equal(payloadRailcarModel);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel2.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});