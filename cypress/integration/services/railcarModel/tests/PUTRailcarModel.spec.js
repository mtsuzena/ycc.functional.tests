import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcarModel = require('../payloads/railcar-model.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
var token = '';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';

describe('PUT Railcar Model', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, RailcarModelBaseUrl, payloadRailcarModel);
        });
    });

    it('YCC-478 - [PUT] - Updates a railcar model by code', () => {
        var payloadRailcarModelUpdated = payloadRailcarModel;
        payloadRailcarModelUpdated.length = 1.0;
        payloadRailcarModelUpdated.tare = 1.0;
        payloadRailcarModelUpdated.axles = 1;

        Requests.updatePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`, payloadRailcarModelUpdated).should((responseUpdateRailcar) => {
            expect(responseUpdateRailcar.status).to.eq(200);

            Requests.getPayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`).should((responseGetRailcar) => {
                if(responseGetRailcar.status == 200){
                    expect(responseGetRailcar.body).to.deep.equal(payloadRailcarModelUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});