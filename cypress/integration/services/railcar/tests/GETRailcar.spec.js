import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcar = require('../payloads/railcar.json');
const payloadRailcar2 = require('../payloads/railcar-2.json');
const payloadRailcarModel = require('../../railcarModel/payloads/railcar-model.json');
const payloadFleetRailcar = require('../../fleet/payloads/fleet_railcar.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');

const RailcarBaseUrl = 'RailCar';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';

var token = '';

describe('GET Railcar', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, RailcarModelBaseUrl, payloadRailcarModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetRailcar);
            Requests.addMultiplePayloads(token, `${RailcarBaseUrl}/railcars`, [payloadRailcar, payloadRailcar2]);
        });
    });

    it('YCC-468 - [GET] - Get all railcars', () => {
        Requests.getAllPayloads(token, RailcarBaseUrl).should((responseGetAllRailcars) => {
            expect(responseGetAllRailcars.status).to.eq(200);

            var responsePayloadRailcar = '';
            var responsePayloadRailcar2 = '';

            responseGetAllRailcars.body.results.map((railcar, i) => {
                if(payloadRailcar.number == railcar.number){
                    responsePayloadRailcar = railcar;
                }
                if(payloadRailcar2.number == railcar.number){
                    responsePayloadRailcar2 = railcar;
                }
            });

            expect(responsePayloadRailcar).to.deep.equal(payloadRailcar);
            expect(responsePayloadRailcar2).to.deep.equal(payloadRailcar2);
        });
    });

    it('YCC-470 - [GET] - Get railcar by number', () => {
        Requests.getPayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`).should((responseGetRailcar) => {
            expect(responseGetRailcar.status).to.eq(200);
            expect(responseGetRailcar.body).to.deep.equal(payloadRailcar);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`);
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar2.number}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});