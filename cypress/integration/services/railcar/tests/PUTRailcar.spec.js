import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcar = require('../payloads/railcar.json');
const payloadRailcarModel = require('../../railcarModel/payloads/railcar-model.json');
const payloadFleetRailcar = require('../../fleet/payloads/fleet_railcar.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');

const RailcarBaseUrl = 'RailCar';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';

var token = '';

describe('PUT Railcar', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, RailcarModelBaseUrl, payloadRailcarModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetRailcar);
            Requests.addPayload(token, RailcarBaseUrl, payloadRailcar);
        });
    });

    it('YCC-471 - [PUT] - Updates a railcar by number', () => {
        var payloadRailcarUpdated = payloadRailcar;
        payloadRailcarUpdated.breakingCondition = !payloadRailcar.breakingCondition;
        payloadRailcarUpdated.label = "CYPRESS_RAILCAR_LABELUPDATED";

        Requests.updatePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`, payloadRailcarUpdated).should((responseUpdateRailcar) => {
            expect(responseUpdateRailcar.status).to.eq(200);
            Requests.getPayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`).should((responseGetRailcar) => {
                if(responseGetRailcar.status == 200){
                    expect(responseGetRailcar.body).to.deep.equal(payloadRailcarUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});