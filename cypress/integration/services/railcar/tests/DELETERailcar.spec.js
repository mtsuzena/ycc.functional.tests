import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as POSTSelectMap from '../../login/requests/POSTSelectMap.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcar = require('../payloads/railcar.json');
const payloadRailcarModel = require('../../railcarModel/payloads/railcar-model.json');
const payloadFleetRailcar = require('../../fleet/payloads/fleet_railcar.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');

const RailcarBaseUrl = 'RailCar';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';

var token = '';

describe('DELETE Railcar', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, RailcarModelBaseUrl, payloadRailcarModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetRailcar);
            Requests.addPayload(token, RailcarBaseUrl, payloadRailcar);
        });
    });

    it('YCC-472 - [DELETE] - Delete an existent railcar by code', () => {
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`).should((responseDeleteRailcar) => {
            expect(responseDeleteRailcar.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});