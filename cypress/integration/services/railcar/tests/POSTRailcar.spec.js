import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as POSTSelectMap from '../../login/requests/POSTSelectMap.request';
import * as Requests from '../../../helpers/requests.request';

const payloadRailcar = require('../payloads/railcar.json');
const payloadRailcar2 = require('../payloads/railcar-2.json');
const payloadRailcar3 = require('../payloads/railcar-3.json');
const payloadRailcarFleetCodeInexistent = require('../payloads/railcar-inexistent-fleet-code.json');
const payloadRailcarModelCodeInexistent = require('../payloads/railcar-inexistent-model-code.json');
const payloadRailcarNotes = require('../payloads/railcar-notes.json');
const payloadRailcarModel = require('../../railcarModel/payloads/railcar-model.json');
const payloadFleetRailcar = require('../../fleet/payloads/fleet_railcar.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadMap = require('../../map/payloads/map.json');

const RailcarBaseUrl = 'RailCar';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';
const MapBaseUrl = 'Map';

var token = '';
var tokenWithMap = '';

describe('POST Railcar', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, RailcarModelBaseUrl, payloadRailcarModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetRailcar);
            Requests.addPayload(token, MapBaseUrl, payloadMap);
            POSTSelectMap.authenticateWithMap(`SelectMap?mapCode=${payloadMap.code}&permission=PerformSwitches`, token).should((responseAuthenticateWithMap) => {
                tokenWithMap = responseAuthenticateWithMap.body.accessToken;
            });
        });
    });

    it('YCC-469 - [POST] - Creates a new railcar', () => {
        Requests.addPayload(token, RailcarBaseUrl, payloadRailcar).should((responseAddRailcar) => {
            expect(responseAddRailcar.status).to.eq(200);
        });
    });

    it('YCC-473 - [POST] - Creates multiple railcars', () => {
        Requests.addPayload(token, RailcarBaseUrl+"/railcars", [payloadRailcar2, payloadRailcar3]).should((responseAddMultipleRailcars) => {
            expect(responseAddMultipleRailcars.status).to.eq(200);
        });
    });

    it('YCC-474 - [POST] - Creates a railcar notes', () => {
        Requests.addPayload(tokenWithMap, RailcarBaseUrl+"/notes", payloadRailcarNotes).should((responseAddRailcarNotes) => {
            expect(responseAddRailcarNotes.status).to.eq(200);
        });
        payloadRailcarNotes.notes = '';
        Requests.addPayload(tokenWithMap, RailcarBaseUrl+"/notes", payloadRailcarNotes).should((responseAddRailcarNotes) => {
            expect(responseAddRailcarNotes.status).to.eq(200);
        });
    });

    it('YCC-561:[POST] - Creates a new railcar - fleetCode inexistent', () => {
        Requests.addPayload(token, RailcarBaseUrl, payloadRailcarFleetCodeInexistent).should((responseAddRailcar) => {
            expect(responseAddRailcar.status).to.eq(400);
        });
    });

    it('YCC-562:[POST] - Creates a new railcar - modelCode inexistent', () => {
        Requests.addPayload(token, RailcarBaseUrl, payloadRailcarModelCodeInexistent).should((responseAddRailcar) => {
            expect(responseAddRailcar.status).to.eq(400);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`);
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar2.number}`);
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar3.number}`);
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
    });

});