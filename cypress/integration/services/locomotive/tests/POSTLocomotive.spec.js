import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as POSTSelectMap from '../../login/requests/POSTSelectMap.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotive = require('../payloads/locomotive.json');
const payloadLocomotive2 = require('../payloads/locomotive-2.json');
const payloadLocomotive3 = require('../payloads/locomotive-3.json');
const payloadLocomotiveFleetCodeInexistent = require('../payloads/locomotive-inexistent-fleet-code.json');
const payloadLocomotiveModelCodeInexistent = require('../payloads/locomotive-inexistent-model-code.json');
const payloadLocomotiveNotes = require('../payloads/locomotive-notes.json');
const payloadLocomotiveModel = require('../../locomotiveModel/payloads/locomotive-model.json');
const payloadFleetLocomotive = require('../../fleet/payloads/fleet_locomotive.json');
const payloadLocomotiveType = require('../../locomotiveType/payloads/locomotive-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadMap = require('../../map/payloads/map.json');

const LocomotiveBaseUrl = 'Locomotive';
const LocomotiveModelBaseUrl = 'LocomotiveModel';
const LocomotiveTypeBaseUrl = 'LocomotiveType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';
const MapBaseUrl = 'Map';

var token = '';
var tokenWithMap = '';

describe('POST Locomotive', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetLocomotive);
            Requests.addPayload(token, MapBaseUrl, payloadMap);
            POSTSelectMap.authenticateWithMap(`SelectMap?mapCode=${payloadMap.code}&permission=PerformSwitches`, token).should((responseAuthenticateWithMap) => {
                tokenWithMap = responseAuthenticateWithMap.body.accessToken;
            });
        });
    });

    it('YCC-444 - [POST] - Creates a new locomotive', () => {
        Requests.addPayload(token, LocomotiveBaseUrl, payloadLocomotive).should((responseAddLocomotive) => {
            expect(responseAddLocomotive.status).to.eq(200);
        });
    });

    it('YCC-448 - [POST] - Creates multiple locomotives', () => {
        Requests.addPayload(token, LocomotiveBaseUrl+"/locomotives", [payloadLocomotive2, payloadLocomotive3]).should((responseAddMultipleLocomotives) => {
            expect(responseAddMultipleLocomotives.status).to.eq(200);
        });
    });

    it('YCC-449 - [POST] - Creates a locomotive notes', () => {
        Requests.addPayload(tokenWithMap, LocomotiveBaseUrl+"/notes", payloadLocomotiveNotes).should((responseAddLocomotiveNotes) => {
            expect(responseAddLocomotiveNotes.status).to.eq(200);
        });
        payloadLocomotiveNotes.notes = '';
        Requests.addPayload(tokenWithMap, LocomotiveBaseUrl+"/notes", payloadLocomotiveNotes).should((responseAddLocomotiveNotes) => {
            expect(responseAddLocomotiveNotes.status).to.eq(200);
        });
    });

    it('YCC-566 - [POST] - Creates a new locomotive - fleetCode inexistent', () => {
        Requests.addPayload(token, LocomotiveBaseUrl, payloadLocomotiveFleetCodeInexistent).should((responseAddLocomotive) => {
            expect(responseAddLocomotive.status).to.eq(400);
        });
    });

    it('YCC-567 - [POST] - Creates a new locomotive - modelCode inexistent', () => {
        Requests.addPayload(token, LocomotiveBaseUrl, payloadLocomotiveModelCodeInexistent).should((responseAddLocomotive) => {
            expect(responseAddLocomotive.status).to.eq(400);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive.number}`);
        Requests.deletePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive2.number}`);
        Requests.deletePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive3.number}`);
        Requests.deletePayloadByCode(token, `${MapBaseUrl}/${payloadMap.code}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`);
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});