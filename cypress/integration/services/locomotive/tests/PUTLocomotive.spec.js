import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotive = require('../payloads/locomotive.json');
const payloadLocomotiveModel = require('../../locomotiveModel/payloads/locomotive-model.json');
const payloadFleetLocomotive = require('../../fleet/payloads/fleet_locomotive.json');
const payloadLocomotiveType = require('../../locomotiveType/payloads/locomotive-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');

const LocomotiveBaseUrl = 'Locomotive';
const LocomotiveModelBaseUrl = 'LocomotiveModel';
const LocomotiveTypeBaseUrl = 'LocomotiveType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';

var token = '';

describe('PUT Locomotive', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetLocomotive);
            Requests.addPayload(token, LocomotiveBaseUrl, payloadLocomotive);
        });
    });

    it('YCC-446 - [PUT] - Updates a locomotive by number', () => {
        var paylodLocomotiveUpdated = payloadLocomotive;
        paylodLocomotiveUpdated.label = "LOCOCYPupdated 350"

        Requests.updatePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive.number}`, paylodLocomotiveUpdated).should((responseUpdateLocomotive) => {
            expect(responseUpdateLocomotive.status).to.eq(200);

            Requests.getPayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive.number}`).should((responseGetLocomotive) => {
                if(responseGetLocomotive.status === 200){
                    expect(responseGetLocomotive.body).to.deep.equal(paylodLocomotiveUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive.number}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`);
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});