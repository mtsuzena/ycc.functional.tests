import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocomotive = require('../payloads/locomotive.json');
const payloadLocomotive2 = require('../payloads/locomotive-2.json');
const payloadLocomotiveModel = require('../../locomotiveModel/payloads/locomotive-model.json');
const payloadFleetLocomotive = require('../../fleet/payloads/fleet_locomotive.json');
const payloadLocomotiveType = require('../../locomotiveType/payloads/locomotive-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');

const LocomotiveBaseUrl = 'Locomotive';
const LocomotiveModelBaseUrl = 'LocomotiveModel';
const LocomotiveTypeBaseUrl = 'LocomotiveType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';

var token = '';

describe('GET Locomotive', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetLocomotive);
            Requests.addPayload(token, LocomotiveBaseUrl, payloadLocomotive);
            Requests.addPayload(token, LocomotiveBaseUrl, payloadLocomotive2);
        });
    });

    it('YCC-443 - [GET] - Get all locomotives', () => {
        Requests.getAllPayloads(token, LocomotiveBaseUrl).should((responseGetAllLocomotives) => {
            expect(responseGetAllLocomotives.status).to.eq(200);

            var responsePayloadLocomotive = '';
            var responsePayloadLocomotive2 = '';

            responseGetAllLocomotives.body.results.map((locomotive) => {
                if(payloadLocomotive.number == locomotive.number){
                    responsePayloadLocomotive = locomotive;
                }
                if(payloadLocomotive2.number == locomotive.number){
                    responsePayloadLocomotive2 = locomotive;
                }
            })

            expect(responsePayloadLocomotive).to.deep.equal(payloadLocomotive);
            expect(responsePayloadLocomotive2).to.deep.equal(payloadLocomotive2);
        });
    });

    it('YCC-445 - [GET] - Get locomotive by number', () => {
        Requests.getPayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive.number}`).should((responseGetLocomotive) => {
            expect(responseGetLocomotive.status).to.eq(200);
            expect(responseGetLocomotive.body).to.deep.equal(payloadLocomotive);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive.number}`);
        Requests.deletePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive2.number}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`);
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});