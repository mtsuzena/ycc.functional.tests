import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocalizationProfile = require('../payloads/localization-profile.json');
var token = '';
const baseUrl = 'LocalizationProfile';

describe('DELETE Localization Profile', () => {

    beforeEach(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, baseUrl, payloadLocalizationProfile);
        });
    });

    it('YCC-442:[DELETE] - Delete an existent localization profile by code', () => {
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadLocalizationProfile.name}`).should((responseRemoveLocalizationProfile) => {
            expect(responseRemoveLocalizationProfile.status).to.eq(200)
        });
    });

});