import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocalizationProfile = require('../payloads/localization-profile.json');
var token = '';
const baseUrl = 'LocalizationProfile';

describe('POST Localization Profile', () => {

    beforeEach(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-439:[POST] - Creates a new localization profile', () => {
        Requests.addPayload(token, baseUrl, payloadLocalizationProfile).should((responseLocalizationProfile) => {
            expect(responseLocalizationProfile.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadLocalizationProfile.name}`)
    });

});