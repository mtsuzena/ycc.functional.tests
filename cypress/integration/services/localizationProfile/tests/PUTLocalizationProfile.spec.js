import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocalizationProfile = require('../payloads/localization-profile.json');
var token = '';
const baseUrl = 'LocalizationProfile';

describe('PUT Localization Profile', () => {

    beforeEach(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, baseUrl, payloadLocalizationProfile);
        });
    });

    it('YCC-441:[PUT] - Updates a localization profile by code', () => {
        var payloadLocalizationProfileUpdated = payloadLocalizationProfile;
        payloadLocalizationProfileUpdated.language = "English";
        payloadLocalizationProfileUpdated.weight = "Pounds";

        Requests.updatePayloadByCode(token, `${baseUrl}/${payloadLocalizationProfile.name}`, payloadLocalizationProfileUpdated).should((responseUpdateLocalizationProfile) => {
            expect(responseUpdateLocalizationProfile.status).to.eq(200);
            Requests.getPayloadByCode(token, `${baseUrl}/${payloadLocalizationProfile.name}`).should((responseGetLocalizationProfileByName) => {
                if(responseGetLocalizationProfileByName.status == 200){
                    expect(responseGetLocalizationProfileByName.body).to.deep.equal(payloadLocalizationProfileUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadLocalizationProfile.name}`);
    });

});


