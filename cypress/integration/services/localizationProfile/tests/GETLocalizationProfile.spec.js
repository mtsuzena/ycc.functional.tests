import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadLocalizationProfile = require('../payloads/localization-profile.json');
const payloadLocalizationProfile2 = require('../payloads/localization-profile-2.json');
var token = '';
const baseUrl = 'LocalizationProfile';

describe('GET Localization Profile', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, baseUrl, payloadLocalizationProfile);
            Requests.addPayload(token, baseUrl, payloadLocalizationProfile2);
        });
    });

    it('YCC-438:[GET] - Get all Localization Profiles', () => {
        Requests.getAllPayloads(token, baseUrl).should((responseGetAllLocalizationProfiles) => {
            expect(responseGetAllLocalizationProfiles.status).to.eq(200);
            expect(responseGetAllLocalizationProfiles.body).to.be.not.null;

            var responsePayloadLocalizationProfile = '';
            var responsePayloadLocalizationProfile2 = '';

            responseGetAllLocalizationProfiles.body.results.map(function(localizationProfile, i){
                if(payloadLocalizationProfile.name == localizationProfile.name){
                    responsePayloadLocalizationProfile = localizationProfile;
                }

                if(payloadLocalizationProfile2.name == localizationProfile.name){
                    responsePayloadLocalizationProfile2 = localizationProfile;
                }
            });

            expect(responsePayloadLocalizationProfile).to.deep.equal(payloadLocalizationProfile);
            expect(responsePayloadLocalizationProfile2).to.deep.equal(payloadLocalizationProfile2);
        });
    });

    it('YCC-440:[GET] - Get localization profile by code', () => {
        Requests.getPayloadByCode(token, `${baseUrl}/${payloadLocalizationProfile.name}`).should((responseGetLocalizationProfileByName) => {
            expect(responseGetLocalizationProfileByName.status).to.eq(200);
            expect(responseGetLocalizationProfileByName.body).to.be.not.null;
            expect(responseGetLocalizationProfileByName.body).to.deep.equal(payloadLocalizationProfile);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadLocalizationProfile.name}`);
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadLocalizationProfile2.name}`);
    });

});