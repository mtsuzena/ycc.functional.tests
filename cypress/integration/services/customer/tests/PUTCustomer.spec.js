import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadCustomer = require('../payloads/customer.json');
const customertBaseUrl = 'Customer';
var token = '';

describe('PUT Customer', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, customertBaseUrl, payloadCustomer);
        });
    });

    it('YCC-413 - [PUT] - Updates a customer by code', () => {
        var payloadCustomerUpdated = payloadCustomer;
        payloadCustomerUpdated.name = "PPGINDUSTINCUPDATED";
        payloadCustomerUpdated.description = "PPG INDUSTRIES INC UPDATED";
        payloadCustomerUpdated.label = "PPG_UP";

        Requests.updatePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`, payloadCustomerUpdated).should((responseUpdatedCustomer) => {
            expect(responseUpdatedCustomer.status).to.eq(200);
            Requests.getPayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`).should((responseGetCustomer) => {
                if(responseGetCustomer.status == 200){
                    expect(responseGetCustomer.body).to.deep.equal(payloadCustomerUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`);
    });

});