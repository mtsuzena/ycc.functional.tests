import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadCustomer = require('../payloads/customer.json');
const customertBaseUrl = 'Customer';
var token = '';

describe('DELETE Customer', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, customertBaseUrl, payloadCustomer);
        });
    });

    it('YCC-414 - [DELETE] - Delete an existent customer by code', () => {
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`).should((responseDeleteCustomer) => {
            expect(responseDeleteCustomer.status).to.eq(200);
        });
    });

});