import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadCustomer = require('../payloads/customer.json');
const payloadCustomer2 = require('../payloads/customer-2.json');
const customertBaseUrl = 'Customer';
var token = '';

describe('POST Customer', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-411 - [POST] - Creates a new customer', () => {
        Requests.addPayload(token, customertBaseUrl, payloadCustomer).should((responseAddCustomer) => {
            expect(responseAddCustomer.status).to.eq(200);
        });
    });

    it('YCC-415 - [POST] - Creates multiple customers', () => {
        Requests.addMultiplePayloads(token, customertBaseUrl+"/customers", [payloadCustomer, payloadCustomer2]).should((responseAddMultipleCustomers) => {
            expect(responseAddMultipleCustomers.status).to.eq(200);
        });
    });

    afterEach(() => {
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`);
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer2.code}`);
    });

});