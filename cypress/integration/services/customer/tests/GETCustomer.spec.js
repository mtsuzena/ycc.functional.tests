import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadCustomer = require('../payloads/customer.json');
const payloadCustomer2 = require('../payloads/customer-2.json');
const customertBaseUrl = 'Customer';
var token = '';

describe('GET Customer', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addMultiplePayloads(token, customertBaseUrl+"/customers", [payloadCustomer, payloadCustomer2]);
        });
    });

    it('YCC-410 - [GET] - Get all customers', () => {
        Requests.getAllPayloads(token, customertBaseUrl).should((responseGetAllCustomers) => {
            expect(responseGetAllCustomers.status).to.eq(200);
            expect(responseGetAllCustomers.body).to.be.not.null;

            var responsePayloadCustomer1 = '';
            var responsePayloadCustomer2 = '';

            responseGetAllCustomers.body.results.map(function(customer, i){
                if(payloadCustomer.code == customer.code){
                    responsePayloadCustomer1 = customer;
                }
                if(payloadCustomer2.code == customer.code){
                    responsePayloadCustomer2 = customer;
                }
            });

            expect(responsePayloadCustomer1).to.deep.equal(payloadCustomer);
            expect(responsePayloadCustomer2).to.deep.equal(payloadCustomer2);
        });
    });

    it('YCC-412 - [GET] - Get customer by code', () => {
        Requests.getPayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`).should((responseGetCustomer) => {
            expect(responseGetCustomer.status).to.eq(200);
            expect(responseGetCustomer.body).to.be.not.null;
            expect(responseGetCustomer.body).to.deep.equal(payloadCustomer);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer.code}`);
        Requests.deletePayloadByCode(token, `${customertBaseUrl}/${payloadCustomer2.code}`);
    });

});