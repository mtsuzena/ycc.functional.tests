import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const yccDataBaseUrl = 'YccData';

var token = '';

describe('GET Ycc Data', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-540 :: Version : 1 :: [GET] - Get all basic data', () => {
        Requests.getAllPayloads(token, `${yccDataBaseUrl}/basic`).should((responseGetAllYccDatas) => {
            expect(responseGetAllYccDatas.status).to.eq(200);
        });
    });

});