import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadGauge = require('../payloads/gauge.json');
const payloadGauge2 = require('../payloads/gauge-2.json');
const baseUrl = 'Gauge';
var token = '';

describe('GET Gauge', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, baseUrl, payloadGauge);
            Requests.addPayload(token, baseUrl, payloadGauge2);
        });
    });

    it('YCC-428 - [GET] - Get all gauges', () => {
        Requests.getAllPayloads(token, baseUrl).should((responseGetAllGauges) => {
            expect(responseGetAllGauges.status).to.eq(200);
            expect(responseGetAllGauges.body).to.be.not.null;

            var responsePayloadGauge = '';
            var responsePayloadGauge2 = '';

            responseGetAllGauges.body.results.map(function(gauge, i){
                if(payloadGauge.code == gauge.code){
                    responsePayloadGauge = gauge;
                }
                if(payloadGauge2.code == gauge.code){
                    responsePayloadGauge2 = gauge;
                }
            });

            expect(responsePayloadGauge).to.deep.equal(payloadGauge);
            expect(responsePayloadGauge2).to.deep.equal(payloadGauge2);
        });
    });

    it('YCC-430 - [GET] - Get gauge by code', () => {
        Requests.getPayloadByCode(token, `${baseUrl}/${payloadGauge.code}`).should((responseGetGaugeByCode) => {
            expect(responseGetGaugeByCode.status).to.eq(200);
            expect(responseGetGaugeByCode.body).to.be.not.null;
            expect(responseGetGaugeByCode.body).to.deep.equal(payloadGauge);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadGauge2.code}`);
    });

});