import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadGauge = require('../payloads/gauge.json');
var token = '';
const baseUrl = 'Gauge';

describe('DELETE Gauge', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, baseUrl, payloadGauge);
        });
    });

    it('YCC-432 - [DELETE] - Delete an existent gauge by code', () => {
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadGauge.code}`).should((responseDeleteGauge) => {
            expect(responseDeleteGauge.status).to.eq(200);
        });
    });

});