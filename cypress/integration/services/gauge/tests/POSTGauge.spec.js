import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadGauge = require('../payloads/gauge.json');
const baseUrl = 'Gauge';
var token = '';

describe('POST Gauge', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-429 - [POST] - Creates a new gauge', () => {
        Requests.addPayload(token, baseUrl, payloadGauge).should((responseAddGauge) => {
            expect(responseAddGauge.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadGauge.code}`);
    });

});