import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadGauge = require('../payloads/gauge.json');
var token = '';
const baseUrl = 'Gauge';

describe('PUT Gauge', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, baseUrl, payloadGauge);
        });
    });

    it('YCC-431 - [PUT] - Updates a gauge by code', () => {
        var payloadGaugeUpdated = payloadGauge;
        payloadGaugeUpdated.name = "CYPRESSUP";
        payloadGaugeUpdated.description = "UCYPRESS";

        Requests.updatePayloadByCode(token, `${baseUrl}/${payloadGauge.code}`, payloadGaugeUpdated).should((responseUpdateGauge) => {
            expect(responseUpdateGauge.status).to.eq(200);

            Requests.getPayloadByCode(token, `${baseUrl}/${payloadGauge.code}`).should((responseGetGaugeByCode) => {
                if(responseGetGaugeByCode.status == 200){
                    expect(responseGetGaugeByCode.body).to.deep.equal(payloadGaugeUpdated);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token, `${baseUrl}/${payloadGauge.code}`);
    });

});