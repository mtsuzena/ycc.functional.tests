import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadTrainOrder = require('../../trainOrder/payloads/train-order.json');
const payloadTrainOrder2 = require('../../trainOrder/payloads/train-order-2.json');
const payloadRailcar = require('../../railcar/payloads/railcar.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadRailcarModel = require('../../railcarModel/payloads/railcar-model.json');
const payloadFleetRailcar = require('../../fleet/payloads/fleet_railcar.json');
const payloadLocomotive = require('../../locomotive/payloads/locomotive.json');
const payloadLocomotiveModel = require('../../locomotiveModel/payloads/locomotive-model.json');
const payloadFleetLocomotive = require('../../fleet/payloads/fleet_locomotive.json');
const payloadLocomotiveType = require('../../locomotiveType/payloads/locomotive-type.json');
const payloadTrainConsist = require('../payloads/train-consist.json');
const payloadTrainConsist2 = require('../payloads/train-consist-2.json');
const payloadTrainSchedule = require('../../trainConsist/payloads/train-schedule.json');

const yardBaseUrl = 'Yard';
const trainOrderBaseUrl = 'TrainOrder';
const RailcarBaseUrl = 'RailCar';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';
const LocomotiveBaseUrl = 'Locomotive';
const LocomotiveModelBaseUrl = 'LocomotiveModel';
const LocomotiveTypeBaseUrl = 'LocomotiveType';
const trainConsistBaseUrl = 'TrainConsist';

var token = '';

describe('GET Train Consist', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, yardBaseUrl, payloadYard2);
            Requests.addPayload(token, trainOrderBaseUrl, payloadTrainOrder);
            Requests.addPayload(token, trainOrderBaseUrl, payloadTrainOrder2);
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, RailcarModelBaseUrl, payloadRailcarModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetRailcar);
            Requests.addPayload(token, RailcarBaseUrl, payloadRailcar);
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetLocomotive);
            Requests.addPayload(token, LocomotiveBaseUrl, payloadLocomotive);
            Requests.addPayload(token, trainConsistBaseUrl, payloadTrainConsist)
            Requests.addPayload(token, trainConsistBaseUrl, payloadTrainConsist2);
        });
    });

    it('YCC-492 :: Version : 1 :: [GET] - Get all train consists', () => {
        Requests.getAllPayloads(token, trainConsistBaseUrl).should((responseGetAllTrainConsist) => {
            expect(responseGetAllTrainConsist.status).to.eq(200);

            var responsePayloadTrainConsist = '';
            var responsePayloadTrainConsist2 = '';

            responseGetAllTrainConsist.body.results.map((trainConsist) => {
                if(payloadTrainConsist.trainId == trainConsist.trainId){
                    responsePayloadTrainConsist = trainConsist;
                }
                if(payloadTrainConsist2.trainId == trainConsist.trainId){
                    responsePayloadTrainConsist2 = trainConsist;
                }
            });
            expect(responsePayloadTrainConsist).to.deep.equal(payloadTrainConsist);
            expect(responsePayloadTrainConsist2).to.deep.equal(payloadTrainConsist2);
        });
    });

    it('YCC-494 :: Version : 1 :: [GET] - Get train consist by trainId', () => {
        Requests.getPayloadByCode(token, `${trainConsistBaseUrl}/${payloadTrainConsist.trainId}`).should((responseGetTrainConsist) => {
            expect(responseGetTrainConsist.body).to.deep.equal(payloadTrainConsist);
            expect(responseGetTrainConsist.status).to.eq(200);
        });
    });

    it('YCC-497 :: Version : 1 :: [GET] - Find trains ids by sitution - Formation', () => {
        Requests.addPayload(token, `${trainConsistBaseUrl}/${payloadTrainConsist.trainId}/TrainSchedule`, payloadTrainSchedule).should((responseTrainSchedule) => {
            Requests.getPayloadByCode(token, `${trainConsistBaseUrl}/situation/Formation`).should((responseGetFormationTrain) => {
                expect(responseGetFormationTrain.status).to.eq(200);
                let check = false;
                responseGetFormationTrain.body.map((train) => {
                    if(train = payloadTrainConsist.trainId){
                        expect(train).to.eq(payloadTrainConsist.trainId);
                        check = true;
                    }
                });
                if(!check){
                    expect(false).to.eq(true);
                }
            });
        });
    });

    it('YCC-499 :: Version : 1 :: [GET] - Find trains ids by sitution - Closed', () => {
        Requests.updateWithoutPayload(token, `${trainConsistBaseUrl}/${payloadTrainConsist.trainId}/TerminateTrain`).should((responseTerminateTrain) => {
            Requests.getPayloadByCode(token, `${trainConsistBaseUrl}/situation/Closed`).should((responseGetClosedTrain) => {
                expect(responseGetClosedTrain.status).to.eq(200);
                let check = false;
                responseGetClosedTrain.body.map((train) => {
                    if(train = payloadTrainConsist.trainId){
                        expect(train).to.eq(payloadTrainConsist.trainId);
                        check = true;
                    }
                });
                if(!check){
                    expect(false).to.eq(true);
                }
            });
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token,`${trainConsistBaseUrl}/${payloadTrainConsist.trainId}`);
        Requests.deletePayloadByCode(token,`${trainConsistBaseUrl}/${payloadTrainConsist2.trainId}`);
        Requests.deletePayloadByCode(token,`${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`);
        Requests.deletePayloadByCode(token,`${trainOrderBaseUrl}/${payloadTrainOrder2.trainId}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard2.code}`);
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive.number}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`);
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });

});