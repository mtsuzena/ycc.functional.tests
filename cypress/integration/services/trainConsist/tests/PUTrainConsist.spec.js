import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../../yard/payloads/yard.json');
const payloadYard2 = require('../../yard/payloads/yard-2.json');
const payloadTrainOrder = require('../../trainOrder/payloads/train-order.json');
const payloadTrainOrder2 = require('../../trainOrder/payloads/train-order-2.json');
const payloadRailcar = require('../../railcar/payloads/railcar.json');
const payloadRailcarType = require('../../railcarType/payloads/railcar-type.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const payloadRailcarModel = require('../../railcarModel/payloads/railcar-model.json');
const payloadFleetRailcar = require('../../fleet/payloads/fleet_railcar.json');
const payloadLocomotive = require('../../locomotive/payloads/locomotive.json');
const payloadLocomotiveModel = require('../../locomotiveModel/payloads/locomotive-model.json');
const payloadFleetLocomotive = require('../../fleet/payloads/fleet_locomotive.json');
const payloadLocomotiveType = require('../../locomotiveType/payloads/locomotive-type.json');
const payloadTrainConsist = require('../payloads/train-consist.json');

const yardBaseUrl = 'Yard';
const trainOrderBaseUrl = 'TrainOrder';
const RailcarBaseUrl = 'RailCar';
const RailcarModelBaseUrl = 'RailCarModel';
const RailcarTypeBaseUrl = 'RailCarType';
const GaugeBaseUrl = 'Gauge';
const fleetBaseUrl = 'Fleet';
const LocomotiveBaseUrl = 'Locomotive';
const LocomotiveModelBaseUrl = 'LocomotiveModel';
const LocomotiveTypeBaseUrl = 'LocomotiveType';
const trainConsistBaseUrl = 'TrainConsist';

var token = '';

describe('PUT Train Consist', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, yardBaseUrl, payloadYard);
            Requests.addPayload(token, yardBaseUrl, payloadYard2);
            Requests.addPayload(token, trainOrderBaseUrl, payloadTrainOrder);
            Requests.addPayload(token, RailcarTypeBaseUrl, payloadRailcarType);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
            Requests.addPayload(token, RailcarModelBaseUrl, payloadRailcarModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetRailcar);
            Requests.addPayload(token, RailcarBaseUrl, payloadRailcar);
            Requests.addPayload(token, LocomotiveTypeBaseUrl, payloadLocomotiveType);
            Requests.addPayload(token, LocomotiveModelBaseUrl, payloadLocomotiveModel);
            Requests.addPayload(token, fleetBaseUrl, payloadFleetLocomotive);
            Requests.addPayload(token, LocomotiveBaseUrl, payloadLocomotive);
            Requests.addPayload(token, trainConsistBaseUrl, payloadTrainConsist);
        });
    });

    it('YCC-495 :: Version : 1 :: [PUT] - Updates a train consist by trainId', () => {
        var responsePayloadTrainConsistUpdated = payloadTrainConsist;
        responsePayloadTrainConsistUpdated.totalGrossWeight = 53125.0;
        responsePayloadTrainConsistUpdated.totalLength = 5125.0;
        responsePayloadTrainConsistUpdated.details = [];

        Requests.updatePayloadByCode(token, `${trainConsistBaseUrl}/${payloadTrainConsist.trainId}`, responsePayloadTrainConsistUpdated).should((responseUpdateTrainConsist) => {
            expect(responseUpdateTrainConsist.status).to.eq(200);

            Requests.getPayloadByCode(token, `${trainConsistBaseUrl}/${payloadTrainConsist.trainId}`).should((responseGetTrainConsist) => {
                if(responseGetTrainConsist.status == 200){
                    expect(responseGetTrainConsist.body).to.deep.equal(responsePayloadTrainConsistUpdated);
                }
            });
        });
    });
    
    it('YCC-501 :: Version : 1 :: [PUT] - Change the Status to Closed for an existing train by trainId', () => {
        Requests.updateWithoutPayload(token, `${trainConsistBaseUrl}/${payloadTrainConsist.trainId}/TerminateTrain`).should((responseTerminateTrain) => {
            expect(responseTerminateTrain.status).to.eq(200);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token,`${trainConsistBaseUrl}/${payloadTrainConsist.trainId}`);
        Requests.deletePayloadByCode(token,`${trainOrderBaseUrl}/${payloadTrainOrder.trainId}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token,`${yardBaseUrl}/${payloadYard2.code}`);
        Requests.deletePayloadByCode(token, `${RailcarBaseUrl}/${payloadRailcar.number}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetRailcar.code}?vehicleType=railcar`);
        Requests.deletePayloadByCode(token, `${RailcarModelBaseUrl}/${payloadRailcarModel.code}`);
        Requests.deletePayloadByCode(token, `${GaugeBaseUrl}/${payloadGauge.code}`);
        Requests.deletePayloadByCode(token, `${RailcarTypeBaseUrl}/${payloadRailcarType.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveBaseUrl}/${payloadLocomotive.number}`);
        Requests.deletePayloadByCode(token, `${fleetBaseUrl}/${payloadFleetLocomotive.code}?vehicleType=locomotive`);
        Requests.deletePayloadByCode(token, `${LocomotiveModelBaseUrl}/${payloadLocomotiveModel.code}`);
        Requests.deletePayloadByCode(token, `${LocomotiveTypeBaseUrl}/${payloadLocomotiveType.code}`);
    });
});