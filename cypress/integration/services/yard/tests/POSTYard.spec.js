import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../payloads/yard.json');
const payloadYard2 = require('../payloads/yard-2.json');
const YardBaseUrl = 'Yard';
var token = '';

describe('POST Yard', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
        });
    });

    it('YCC-531 - [POST] - Creates a new yard', () => {
        Requests.addPayload(token, YardBaseUrl, payloadYard).should((responseAddYard) => {
            expect(responseAddYard.status).to.eq(200);
        });
    });

    it('YCC-535 - [POST] - Creates multiple yards', () => {
        Requests.addMultiplePayloads(token, `${YardBaseUrl}/yards`, [payloadYard, payloadYard2]).should((responseAddMultipleYards) => {
            expect(responseAddMultipleYards.status).to.eq(200);
        });
    });

    afterEach(() => {
        Requests.deletePayloadByCode(token,`${YardBaseUrl}/${payloadYard.code}`);
    });

    after(() => {
        Requests.deletePayloadByCode(token,`${YardBaseUrl}/${payloadYard2.code}`);
    });

});