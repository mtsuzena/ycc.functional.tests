import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../payloads/yard.json');
const payloadYard2 = require('../payloads/yard-2.json');
const YardBaseUrl = 'Yard';
var token = '';

describe('GET Yard', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addMultiplePayloads(token, `${YardBaseUrl}/yards`, [payloadYard, payloadYard2]);
        });
    });

    it('YCC-530 - [GET] - Get all yards', () => {
        Requests.getAllPayloads(token, YardBaseUrl).should((responseGetAllYards) => {
            expect(responseGetAllYards.status).to.eq(200);
            expect(responseGetAllYards.body).to.be.not.null;

            var responsePayloadYard = '';
            var responsePayloadYard2 = '';

            responseGetAllYards.body.results.map(function(yard, i){
                if(payloadYard.code == yard.code){
                    responsePayloadYard = yard;
                }
                if(payloadYard2.code == yard.code){
                    responsePayloadYard2 = yard;
                }
            });

            expect(responsePayloadYard).to.deep.equal(payloadYard);
            expect(responsePayloadYard2).to.deep.equal(payloadYard2);
        });
    });

    it('YCC-532 - [GET] - Get yard by code', () => {
        Requests.getPayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`).should((responseGetYardByCode) => {
            expect(responseGetYardByCode.status).to.eq(200);
            expect(responseGetYardByCode.body).to.be.not.null;
            expect(responseGetYardByCode.body).to.deep.equal(payloadYard);
        });
    });

    after(() => {
        Requests.deletePayloadByCode(token,`${YardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token,`${YardBaseUrl}/${payloadYard2.code}`);
    });

});