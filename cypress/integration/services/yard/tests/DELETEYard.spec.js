import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../payloads/yard.json');
const YardBaseUrl = 'Yard';
var token = '';

describe('DELETE Yard', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
        });
    });

    it('YCC-534 - [DELETE] - Delete an existent yard by code', () => {
        Requests.deletePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`).should((responseDeleteYard) => {
            expect(responseDeleteYard.status).to.eq(200);
        });
    });

});