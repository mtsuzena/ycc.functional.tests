import * as POSTAuthenticate from '../../login/requests/POSTAuthenticate.request';
import * as Requests from '../../../helpers/requests.request';

const payloadYard = require('../payloads/yard.json');
const payloadGauge = require('../../gauge/payloads/gauge.json');
const YardBaseUrl = 'Yard';
const GaugeBaseUrl = 'Gauge';
var payloadYardUpdated = '';
var token = '';

describe('PUT Yard', () => {

    before(() => {
        POSTAuthenticate.authenticate().should((responseAuthenticate) => {
            token = responseAuthenticate.body.accessToken;
            Requests.addPayload(token, YardBaseUrl, payloadYard);
            Requests.addPayload(token, GaugeBaseUrl, payloadGauge);
        });
    });

    it('YCC-533 - [PUT] - Updates a yard by code', () => {
        payloadYardUpdated = payloadYard;
        payloadYardUpdated.name = "PRONAUP, SL";
        payloadYardUpdated.description = "KCS-PRONAPAUP, SL";
        payloadYardUpdated.label = "PROUP";

        Requests.updatePayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`, payloadYardUpdated).should((responseUpdateYard) => {
            expect(responseUpdateYard.status).to.eq(200);

            Requests.getPayloadByCode(token, `${YardBaseUrl}/${payloadYard.code}`).should((responseGetYardByCode) => {
                if(responseGetYardByCode.status == 200){
                    expect(responseGetYardByCode.body).to.deep.equal(payloadYardUpdated);
                }
            });
        });

    });

    after(() => {
        Requests.deletePayloadByCode(token,`${YardBaseUrl}/${payloadYard.code}`);
        Requests.deletePayloadByCode(token,`${GaugeBaseUrl}/${payloadGauge.code}`);
    });

});