/// <reference types="cypress" />

function addPayload(token, url, payload){
    return cy.request({
        method: 'POST',
        url: url,
        failOnStatusCode: false,
        body: payload,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}

function addMultiplePayloads(token, url, payloads){
    return cy.request({
        method: 'POST',
        url: url,
        failOnStatusCode: false,
        body: payloads,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}

function getAllPayloads(token, url){
    return cy.request({
        method: 'GET',
        url: url,
        failOnStatusCode: false,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}

function getPayloadByCode(token, url){
    return cy.request({
        method: 'GET',
        url: url,
        failOnStatusCode: false,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}

function updatePayloadByCode(token, url, payload){
    return cy.request({
        method: 'PUT',
        url: url,
        failOnStatusCode: false,
        body: payload,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}

function updateWithoutPayload(token, url){
    return cy.request({
        method: 'PUT',
        url: url,
        failOnStatusCode: false,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}

function deletePayloadByCode(token, url){
    return cy.request({
        method: 'DELETE',
        url: url,
        failOnStatusCode: false,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}

export { addPayload, addMultiplePayloads, getAllPayloads, getPayloadByCode, updatePayloadByCode, deletePayloadByCode, updateWithoutPayload };